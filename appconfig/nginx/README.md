# PARROT Local Application Product Release

The <strong>PARROT Local Application</strong> is a web application (that means that its user interface runs in your web browser) that you can install and run locally on your computer. The application provides different modules that allow you to

- run ML-models
- visualize CT images
- visualize, predict or modify segmentation information
- visualize, evaluate, and compare dose treatment plans.

You can load patient studies into the application in the form of DICOM files. Internally, the application uses an nginx server to run the web application, a Python server for the prediction of the integrated trained AI model, an Orthanc server for the storage of the study data. The installation package includes all servers. However, the Orthanc server is a manual installation. The installation instructions can be found below.

> :warning: The Orthanc server will run on [port 8042](http://localhost:8042/) of your local computer and the nginx server on [port 2000](http://localhost:2000/).


# Stopping the application server

- The nginx server will continue running, even when you close the web browser window. To completely stop the server, double click the <strong>'Stop_PARROT.bat'</strong> file in the <strong>'parrot'</strong> folder (or stop the nginx processes in the Task Manager).
- The Orthanc server runs as a Windows service and has to be stopped in the <strong>'Task Manager'</strong> (in the tab <strong>'Services'</strong>). Don't forget to start the service again when you want to use the PARROT application.

# Directory structure

The <strong>'parrot'</strong> folder contains the following files:

- <strong>'aiIntegration'</strong> contains the integrated trained AI models, the Python server environment and the API server directories
- <strong>'dist'</strong> contains the PARROT web application
- <strong>'logs'</strong> contains the log files of the nginx server
- <strong>'temp'</strong> contains the temporary files of nginx
- <strong>'tempai'</strong> contains the results of running the Python code. The user can configure the path for AI integration.
- <strong>'favicon.ico'</strong> contains the the image of the application.
- <strong>'License'</strong> contains the License of the application.
- <strong>'nginx.exe'</strong> starts the nginx server (see the instructions above).
- <strong>'script-installer.iss'</strong> contains the script to create the installer.
- <strong>'Start_PARROT.bat'</strong> start the nginx server (see the instructions above).
- <strong>'Stop_PARROT.bat'</strong> stop the ngnix server (see the instructions above).
- <strong>'unins000.exe'</strong> uninstalls the application from the computer.
