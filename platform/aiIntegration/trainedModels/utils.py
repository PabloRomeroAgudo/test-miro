import json
import os
import os.path
import pydicom
import copy
import sys
import yaml
import requests
import urllib.request
import numpy as np
import nibabel as nib
from multiprocessing.dummy import Pool as ThreadPool
from opentps.core.data._transform3D import Transform3D
from opentps.core.io.dataLoader import readData

orthancURL = "http://localhost:8042"

def downloadInstance(instanceID, dicompath, modality):
    urllib.request.urlretrieve(orthancURL+"/instances/"+instanceID+"/file",  os.path.join(dicompath, instanceID + "_" + modality + ".dcm"))

def downloadSeriesInstanceByModality(seriesInstanceUid, dicompath, modality):    
    orthanQuery = {
        "Level": "Instance",
        "Query": {
            "Modality": modality,
            "SeriesInstanceUID": seriesInstanceUid
        }}
    try:
        # Get list of modality instances for the study
        response = requests.post(
            orthancURL+"/tools/find", json=orthanQuery).json()
        print(f"{len(response)} {modality} instances found. Loading...")
                
        with ThreadPool(50) as pool:
            pool.map(lambda x: downloadInstance(x, dicompath, modality), response)
            
    except Exception as e:
        sys.exit("Downloading dicom files failed "+str(e))

#
# Uploads the dicom file to the Orthanc server
# Throws an exception if error
#
def uploadDicomToOrthanc(filePath):
    with open(filePath, 'rb') as contents:
        headers = {'content-type': 'application/dicom'}
        response = requests.post(orthancURL+'/instances',
                                 data=contents, headers=headers)
        if not response.ok:
            raise response.reason
        
'''
Delete dicom file
'''
def deleteFileByPath(file_path):
    if os.path.exists(file_path):
        os.remove(file_path)
    else:
        print("The system cannot find the file specified")

# Create directory
def createdir(path):
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=True)
    print("Created a new folder: ", path)
    
'''
Write data to file
'''
def writeToFile(filename, data):
    logfile = r"..\..\..\dist\parrot-app\logs" + "\\"
    if not os.path.exists(os.path.join(testLogFile, filename)):
        with open(filename, 'w') as outfile:
            print(data, file=outfile)
    else:
        os.remove(os.path.join(logfile, filename))
        writeToFile(filename, data)

'''
Check the modality of the dicom file
'''
def checkFileValid(filePath, modality):
    if (os.path.isfile(filePath)):
        dcm = pydicom.read_file(filePath)
        if dcm and dcm.Modality == modality:
            return "True"
        else:
            return "False"
    else:
        return "FileNotFound"

'''
Get dicom file by the parameters: file path, file name, modality and series instance UID
''' 
def getDicomFileByFilename(fpath, fileName, modality, seriesInstanceUID):
    for root, dirs, files in os.walk(fpath):
        for f in files:
            if f == fileName & (f.endswith('.dcm') or f.endswith('.DCM')):
                filePath = os.path.join(root, f)
                dcm = pydicom.read_file(filePath)
                if dcm.Modality == modality:
                    if dcm.SeriesInstanceUID == seriesInstanceUID:
                        return dcm
    return null


'''
Get dicom file by the parameters: file path, file name, modality and series instance UID
''' 
def getDicomFile(fpath, modality, seriesInstanceUID):
    for root, dirs, files in os.walk(fpath):
        for f in files:
            if (f.endswith('.dcm') or f.endswith('.DCM')):
                filePath = os.path.join(root, f)
                dcm = pydicom.read_file(filePath)
                if dcm.Modality == modality:
                    if dcm.SeriesInstanceUID == seriesInstanceUID:
                        return dcm
    return null

'''
Get the related series instance UID from the modality 'REG' dicom file for related "MR" modality data set 
'''
def getSeriesInstanceUIDFromRegDicom(dir_reg_dicom, regSeriesInstanceUID):
    for root, dirs, files, in os.walk(dir_reg_dicom):
        for f in files:
            if f.endswith('.dcm') or f.endswith('.DCM'):
                filePath = os.path.join(root, f)
                dcm = pydicom.read_file(filePath)
                if dcm.Modality == 'REG' and dcm.SeriesInstanceUID == regSeriesInstanceUID:
                    if hasattr(dcm, 'StudiesContainingOtherReferencedInstancesSequence') and len(dcm.StudiesContainingOtherReferencedInstancesSequence)>0:
                        seq = dcm.StudiesContainingOtherReferencedInstancesSequence[0]
                        if hasattr(seq, 'ReferencedSeriesSequence') and len(seq.ReferencedSeriesSequence)>0:
                            refSeq = seq.ReferencedSeriesSequence[0]
                            if hasattr(refSeq, 'SeriesInstanceUID') and refSeq.SeriesInstanceUID != "":
                                return refSeq.SeriesInstanceUID

    return null

def getCTSeriesInstanceUIDFromRegDicom(dir_reg_dicom, regSeriesInstanceUID):
    for root, dirs, files, in os.walk(dir_reg_dicom):
        for f in files:
            if f.endswith('.dcm') or f.endswith('.DCM'):
                filePath = os.path.join(root, f)
                dcm = pydicom.read_file(filePath)
                if dcm.Modality == 'REG' and dcm.SeriesInstanceUID == regSeriesInstanceUID:
                    if hasattr(dcm, 'ReferencedSeriesSequence') and len(dcm.ReferencedSeriesSequence)>0:
                        refSerSeq = dcm.ReferencedSeriesSequence[0]
                        if hasattr(refSerSeq, 'SeriesInstanceUID') and refSerSeq.SeriesInstanceUID != "":
                            return refSerSeq.SeriesInstanceUID
                        
    return null

'''
Create the new temporary directory
'''
def createTemporaryDirectory(dirPath):
    isExists = os.path.exists(dirPath)
    if not isExists:
        os.mkdir(dirPath)
        print('Created the temporary directory:', dirPath)
    else: 
        print('Temporary directory exists already')

'''
Delete the file by file path
'''
def deleteFileByPath(file_path):
    if os.path.exists(file_path):
        os.remove(file_path)
    else:
        print("The system cannot find the file specified")
        
'''
The transformation function for the data set with modality "MR" and "REG"
'''

def regMatrixTransformation(dir_mr_dicom, reg_file_path, regSeriesInstanceUID, CT):
          
    # get REG dicom file and matrix transformation
    regDcm = getDicomFile(reg_file_path, 'REG', regSeriesInstanceUID)
    
    classUID_mr = '1.2.840.10008.5.1.4.1.1.4'
    
    # find the registration sequence to use (i.e. the one associated to the mr)
    for i in range(len(regDcm.RegistrationSequence)):
        if regDcm.RegistrationSequence[i].ReferencedImageSequence[0].ReferencedSOPClassUID == classUID_mr:
            seq_i = i     
            reg_type = regDcm.RegistrationSequence[seq_i].MatrixRegistrationSequence[0].MatrixSequence[0].FrameOfReferenceTransformationMatrixType
            reg_matrix =  regDcm.RegistrationSequence[seq_i].MatrixRegistrationSequence[0].MatrixSequence[0].FrameOfReferenceTransformationMatrix
            
            #print(reg_type)
            tformMatrix = np.array(reg_matrix).reshape(4, 4)
            # get inverse
            tformMatrix_1 = np.linalg.inv(tformMatrix)   
            
            # call openTPS function - Damien's code
            transform3D = Transform3D()
            transform3D.setCenter('dicomOrigin') # other option is 'imgCenter' but in this case we should use dicomOrigin
            transform3D.setMatrix4x4(tformMatrix_1)
            
    data = readData(dir_mr_dicom)
    mr_data = data[0]
    
    # Perform registration
    mr_reg = transform3D.deformData(mr_data, outputBox='keepAll')

    # resample to CT grid
    mr_reg.resample(CT.PixelSpacing, CT.GridSize, CT.ImagePositionPatient)
        
    return mr_reg

# ##################################################################################
class PatientList:
    
    def __init__(self):
        self.list = []

    def find_CT_image(self, display_id):
        count = -1
        for patient_id in range(len(self.list)):
            for ct_id in range(len(self.list[patient_id].CTimages)):
                if (self.list[patient_id].CTimages[ct_id].isLoaded == 1):
                    count += 1
                if (count == display_id):
                    break
            if (count == display_id):
                break

        return patient_id, ct_id

    def find_dose_image(self, display_id):
        count = -1
        for patient_id in range(len(self.list)):
            for dose_id in range(len(self.list[patient_id].RTdoses)):
                if (self.list[patient_id].RTdoses[dose_id].isLoaded == 1):
                    count += 1
                if (count == display_id):
                    break
            if (count == display_id):
                break

        return patient_id, dose_id

    def find_contour(self, ROIName):
        for patient_id in range(len(self.list)):
            for struct_id in range(len(self.list[patient_id].RTstructs)):
                if (self.list[patient_id].RTstructs[struct_id].isLoaded == 1):
                    for contour_id in range(len(self.list[patient_id].RTstructs[struct_id].Contours)):
                        if (self.list[patient_id].RTstructs[struct_id].Contours[contour_id].ROIName == ROIName):
                            return patient_id, struct_id, contour_id

    def list_dicom_files(self, folder_path, recursive):
        file_list = os.listdir(folder_path)
        # print("len file_list", len(file_list), "folderpath",folder_path)
        for file_name in file_list:
            file_path = os.path.join(folder_path, file_name)

            # folders
            if os.path.isdir(file_path):
                if recursive == True:
                    subfolder_list = self.list_dicom_files(file_path, True)
                    # join_patient_lists(Patients, subfolder_list)

            # files
            elif os.path.isfile(file_path):

                try:
                    dcm = pydicom.dcmread(file_path)
                except:
                    print("Invalid Dicom file: " + file_path)
                    continue

                patient_id = next((x for x, val in enumerate(
                    self.list) if val.PatientInfo.PatientID == dcm.PatientID), -1)

                if patient_id == -1:
                    Patient = PatientData()
                    Patient.PatientInfo.PatientID = dcm.PatientID
                    Patient.PatientInfo.PatientName = str(dcm.PatientName)
                    Patient.PatientInfo.PatientBirthDate = dcm.PatientBirthDate
                    Patient.PatientInfo.PatientSex = dcm.PatientSex
                    self.list.append(Patient)
                    patient_id = len(self.list) - 1

                # Dicom CT
                if dcm.SOPClassUID == "1.2.840.10008.5.1.4.1.1.2":
                    ct_id = next((x for x, val in enumerate(
                        self.list[patient_id].CTimages) if val.SeriesInstanceUID == dcm.SeriesInstanceUID), -1)
                    if ct_id == -1:
                        CT = CTimage()
                        CT.SeriesInstanceUID = dcm.SeriesInstanceUID
                        CT.SOPClassUID == "1.2.840.10008.5.1.4.1.1.2"
                        CT.PatientInfo = self.list[patient_id].PatientInfo
                        CT.StudyInfo = StudyInfo()
                        CT.StudyInfo.StudyInstanceUID = dcm.StudyInstanceUID
                        CT.StudyInfo.StudyID = dcm.StudyID
                        CT.StudyInfo.StudyDate = dcm.StudyDate
                        CT.StudyInfo.StudyTime = dcm.StudyTime
                        if (hasattr(dcm, 'SeriesDescription') and dcm.SeriesDescription != ""):
                            CT.ImgName = dcm.SeriesDescription
                        else:
                            CT.ImgName = dcm.SeriesInstanceUID
                        self.list[patient_id].CTimages.append(CT)
                        ct_id = len(self.list[patient_id].CTimages) - 1

                    self.list[patient_id].CTimages[ct_id].DcmFiles.append(
                        file_path)
                elif dcm.SOPClassUID == "1.2.840.10008.5.1.4.1.1.4":
                        mr_id = next((x for x, val in enumerate(self.list[patient_id].MRimages) if val.SeriesInstanceUID == dcm.SeriesInstanceUID), -1)
                        if mr_id == -1:
                            MR = MRimage()
                            MR.SeriesInstanceUID = dcm.SeriesInstanceUID
                            MR.SOPClassUID == "1.2.840.10008.5.1.4.1.1.4"
                            MR.PatientInfo = self.list[patient_id].PatientInfo
                            MR.StudyInfo = StudyInfo()
                            MR.StudyInfo.StudyInstanceUID = dcm.StudyInstanceUID
                            MR.StudyInfo.StudyID = dcm.StudyID
                            MR.StudyInfo.StudyDate = dcm.StudyDate
                            MR.StudyInfo.StudyTime = dcm.StudyTime
                            if(hasattr(dcm, 'SeriesDescription') and dcm.SeriesDescription != ""): MR.ImgName = dcm.SeriesDescription
                            else: MR.ImgName = dcm.SeriesInstanceUID
                            self.list[patient_id].MRimages.append(MR)
                            mr_id = len(self.list[patient_id].MRimages) - 1

                        self.list[patient_id].MRimages[mr_id].DcmFiles.append(file_path)
                else:
                    print("Unknown SOPClassUID " +
                          dcm.SOPClassUID + " for file " + file_path)
            # other
            else:
                print("Unknown file type " + file_path)

    def print_patient_list(self):
        print("")
        for patient in self.list:
            patient.print_patient_info()

        print("")


class PatientData:

    def __init__(self):
        self.PatientInfo = PatientInfo()
        self.CTimages = []
        self.MRimages = []

    def print_patient_info(self, prefix=""):
        print("")
        print(prefix + "PatientName: " + self.PatientInfo.PatientName)
        print(prefix + "PatientID: " + self.PatientInfo.PatientID)

        for ct in self.CTimages:
            print("")
            ct.print_CT_info(prefix + "   ")
            
        for mr in self.MRimages:
            print("")
            mr.print_MR_info(prefix + "  ")

    def import_patient_data(self, newvoxelsize=None):
        # import CT images
        for i, ct in enumerate(self.CTimages):
            if (ct.isLoaded == 1):
                continue
            ct.import_Dicom_CT()
        # Resample CT images
        for i, ct in enumerate(self.CTimages):
            ct.resample_CT(newvoxelsize)
            
        # import MR images
        for i, mr in enumerate(self.MRimages):
            if (mr.isLoaded == 1):
                continue
            mr.import_Dicom_MR(newvoxelsize)
        # Resample MR images
        # for i,mr in enumerate(self.MRimages):
        #     mr.resample_MR(newvoxelsize)

class PatientInfo:

    def __init__(self):
        self.PatientID = ''
        self.PatientName = ''
        self.PatientBirthDate = ''
        self.PatientSex = ''


class StudyInfo:

    def __init__(self):
        self.StudyInstanceUID = ''
        self.StudyID = ''
        self.StudyDate = ''
        self.StudyTime = ''


class CTimage:

    def __init__(self):
        self.SeriesInstanceUID = ""
        self.PatientInfo = {}
        self.StudyInfo = {}
        self.FrameOfReferenceUID = ""
        self.ImgName = ""
        self.SOPClassUID = ""
        self.DcmFiles = []
        self.isLoaded = 0

    def print_CT_info(self, prefix=""):
        print(prefix + "CT series: " + self.SeriesInstanceUID)
        for ct_slice in self.DcmFiles:
            print(prefix + "   " + ct_slice)

    def resample_CT(self, newvoxelsize):
        ct = self.Image
        # Rescaling to the newvoxelsize if given in parameter
        if newvoxelsize is not None:
            source_shape = self.GridSize
            voxelsize = self.PixelSpacing
            # print("self.ImagePositionPatient",self.ImagePositionPatient, "source_shape",source_shape,"voxelsize",voxelsize)
            VoxelX_source = self.ImagePositionPatient[0] + \
                np.arange(source_shape[0])*voxelsize[0]
            VoxelY_source = self.ImagePositionPatient[1] + \
                np.arange(source_shape[1])*voxelsize[1]
            VoxelZ_source = self.ImagePositionPatient[2] + \
                np.arange(source_shape[2])*voxelsize[2]

            target_shape = np.ceil(np.array(source_shape).astype(
                float)*np.array(voxelsize).astype(float)/newvoxelsize).astype(int)
            VoxelX_target = self.ImagePositionPatient[0] + \
                np.arange(target_shape[0])*newvoxelsize[0]
            VoxelY_target = self.ImagePositionPatient[1] + \
                np.arange(target_shape[1])*newvoxelsize[1]
            VoxelZ_target = self.ImagePositionPatient[2] + \
                np.arange(target_shape[2])*newvoxelsize[2]
            # print("source_shape",source_shape,"target_shape",target_shape)
            if (all(source_shape == target_shape) and np.linalg.norm(np.subtract(voxelsize, newvoxelsize) < 0.001)):
                print("Image does not need filtering")
            else:
                # anti-aliasing filter
                sigma = [0, 0, 0]
                if (newvoxelsize[0] > voxelsize[0]):
                    sigma[0] = 0.4 * (newvoxelsize[0]/voxelsize[0])
                if (newvoxelsize[1] > voxelsize[1]):
                    sigma[1] = 0.4 * (newvoxelsize[1]/voxelsize[1])
                if (newvoxelsize[2] > voxelsize[2]):
                    sigma[2] = 0.4 * (newvoxelsize[2]/voxelsize[2])

                if (sigma != [0, 0, 0]):
                    print("Image is filtered before downsampling")
                    ct = scipy.ndimage.gaussian_filter(ct, sigma)

                xi = np.array(np.meshgrid(
                    VoxelX_target, VoxelY_target, VoxelZ_target))
                xi = np.rollaxis(xi, 0, 4)
                xi = xi.reshape((xi.size // 3, 3))

                # get resized ct
                ct = scipy.interpolate.interpn((VoxelX_source, VoxelY_source, VoxelZ_source), ct, xi, method='linear',
                                               fill_value=-1000, bounds_error=False).reshape(target_shape).transpose(1, 0, 2)

            self.PixelSpacing = newvoxelsize
        self.GridSize = list(ct.shape)
        self.NumVoxels = self.GridSize[0] * self.GridSize[1] * self.GridSize[2]
        self.Image = ct
        # print("self.ImagePositionPatient",self.ImagePositionPatient, "self.GridSize[0]",self.GridSize[0],"self.PixelSpacing",self.PixelSpacing)

        self.VoxelX = self.ImagePositionPatient[0] + \
            np.arange(self.GridSize[0])*self.PixelSpacing[0]
        self.VoxelY = self.ImagePositionPatient[1] + \
            np.arange(self.GridSize[1])*self.PixelSpacing[1]
        self.VoxelZ = self.ImagePositionPatient[2] + \
            np.arange(self.GridSize[2])*self.PixelSpacing[2]
        self.isLoaded = 1

    def import_Dicom_CT(self):

        if (self.isLoaded == 1):
            print("Warning: CT serries " +
                  self.SeriesInstanceUID + " is already loaded")
            return

        images = []
        SOPInstanceUIDs = []
        SliceLocation = np.zeros(len(self.DcmFiles), dtype='float')

        for i in range(len(self.DcmFiles)):
            file_path = self.DcmFiles[i]
            dcm = pydicom.dcmread(file_path)

            if (hasattr(dcm, 'SliceLocation') and abs(dcm.SliceLocation - dcm.ImagePositionPatient[2]) > 0.001):
                print("WARNING: SliceLocation (" + str(dcm.SliceLocation) +
                      ") is different than ImagePositionPatient[2] (" + str(dcm.ImagePositionPatient[2]) + ") for " + file_path)

            SliceLocation[i] = float(dcm.ImagePositionPatient[2])
            images.append(dcm.pixel_array * dcm.RescaleSlope +
                          dcm.RescaleIntercept)
            SOPInstanceUIDs.append(dcm.SOPInstanceUID)

        # sort slices according to their location in order to reconstruct the 3d image
        sort_index = np.argsort(SliceLocation)
        SliceLocation = SliceLocation[sort_index]
        SOPInstanceUIDs = [SOPInstanceUIDs[n] for n in sort_index]
        images = [images[n] for n in sort_index]
        ct = np.dstack(images).astype("float32")

        if ct.shape[0:2] != (dcm.Rows, dcm.Columns):
            print("WARNING: GridSize " + str(ct.shape[0:2]) + " different from Dicom Rows (" + str(
                dcm.Rows) + ") and Columns (" + str(dcm.Columns) + ")")

        MeanSliceDistance = (
            SliceLocation[-1] - SliceLocation[0]) / (len(images)-1)
        if (abs(MeanSliceDistance - dcm.SliceThickness) > 0.001):
            print("WARNING: MeanSliceDistance (" + str(MeanSliceDistance) +
                  ") is different from SliceThickness (" + str(dcm.SliceThickness) + ")")

        self.FrameOfReferenceUID = dcm.FrameOfReferenceUID
        self.ImagePositionPatient = [float(dcm.ImagePositionPatient[0]), float(
            dcm.ImagePositionPatient[1]), SliceLocation[0]]
        self.PixelSpacing = [float(dcm.PixelSpacing[0]), float(
            dcm.PixelSpacing[1]), MeanSliceDistance]
        self.GridSize = list(ct.shape)
        self.NumVoxels = self.GridSize[0] * self.GridSize[1] * self.GridSize[2]
        self.Image = ct
        self.SOPInstanceUIDs = SOPInstanceUIDs
        self.VoxelX = self.ImagePositionPatient[0] + \
            np.arange(self.GridSize[0])*self.PixelSpacing[0]
        self.VoxelY = self.ImagePositionPatient[1] + \
            np.arange(self.GridSize[1])*self.PixelSpacing[1]
        self.VoxelZ = self.ImagePositionPatient[2] + \
            np.arange(self.GridSize[2])*self.PixelSpacing[2]
        self.isLoaded = 1


class MRimage:
    
  def __init__(self):
    self.SeriesInstanceUID = ""
    self.PatientInfo = {}
    self.StudyInfo = {}
    self.FrameOfReferenceUID = ""
    self.ImgName = ""
    self.SOPClassUID = ""
            
    self.DcmFiles = []
    self.isLoaded = 0
    
    
  def print_MR_info(self, prefix=""):
    print(prefix + "MR series: " + self.SeriesInstanceUID)
    for mr_slice in self.DcmFiles:
      print(prefix + "   " + mr_slice)
      
  def import_Dicom_MR(self, newvoxelsize):
    
    if(self.isLoaded == 1):
      print("Warning: CT series " + self.SeriesInstanceUID + " is already loaded")
      return
  
    images = []
    SOPInstanceUIDs = []
    SliceLocation = np.zeros(len(self.DcmFiles), dtype='float')

    for i in range(len(self.DcmFiles)):
      file_path = self.DcmFiles[i]
      dcm = pydicom.dcmread(file_path)

      if(hasattr(dcm, 'SliceLocation') and abs(dcm.SliceLocation - dcm.ImagePositionPatient[2]) > 0.001):
        print("WARNING: SliceLocation (" + str(dcm.SliceLocation) + ") is different than ImagePositionPatient[2] (" + str(dcm.ImagePositionPatient[2]) + ") for " + file_path)

      SliceLocation[i] = float(dcm.ImagePositionPatient[2])
      images.append(dcm.pixel_array)# * dcm.RescaleSlope + dcm.RescaleIntercept)
      SOPInstanceUIDs.append(dcm.SOPInstanceUID)

    # sort slices according to their location in order to reconstruct the 3d image
    sort_index = np.argsort(SliceLocation)
    SliceLocation = SliceLocation[sort_index]
    SOPInstanceUIDs = [SOPInstanceUIDs[n] for n in sort_index]
    images = [images[n] for n in sort_index]
    mr = np.dstack(images).astype("float32")

    if mr.shape[0:2] != (dcm.Rows, dcm.Columns):
      print("WARNING: GridSize " + str(mr.shape[0:2]) + " different from Dicom Rows (" + str(dcm.Rows) + ") and Columns (" + str(dcm.Columns) + ")")

    MeanSliceDistance = (SliceLocation[-1] - SliceLocation[0]) / (len(images)-1)
    if(abs(MeanSliceDistance - dcm.SliceThickness) > 0.001):
      print("WARNING: MeanSliceDistance (" + str(MeanSliceDistance) + ") is different from SliceThickness (" + str(dcm.SliceThickness) + ")")
    
    # Rescaling to the newvoxelsize if given in parameter
    if newvoxelsize is not None:
      source_shape = list(mr.shape)
      
      voxelsize = [float(dcm.PixelSpacing[0]), float(dcm.PixelSpacing[1]), MeanSliceDistance]
      VoxelX_source = dcm.ImagePositionPatient[0] + np.arange(source_shape[0])*voxelsize[0]
      VoxelY_source = dcm.ImagePositionPatient[1] + np.arange(source_shape[1])*voxelsize[1]
      VoxelZ_source = dcm.ImagePositionPatient[2] + np.arange(source_shape[2])*voxelsize[2]

      target_shape = np.ceil(np.array(source_shape).astype(float)*np.array(voxelsize).astype(float)/newvoxelsize).astype(int)
      VoxelX_target = dcm.ImagePositionPatient[0] + np.arange(target_shape[0])*newvoxelsize[0]
      VoxelY_target = dcm.ImagePositionPatient[1] + np.arange(target_shape[1])*newvoxelsize[1]
      VoxelZ_target = dcm.ImagePositionPatient[2] + np.arange(target_shape[2])*newvoxelsize[2]
      
      if(all(source_shape == target_shape) and np.linalg.norm(np.subtract(voxelsize, newvoxelsize) < 0.001)):
        print("Image does not need filtering")
      else:
        # anti-aliasing filter
        sigma = [0, 0, 0]
        if(newvoxelsize[0] > voxelsize[0]): sigma[0] = 0.4 * (newvoxelsize[0]/voxelsize[0])
        if(newvoxelsize[1] > voxelsize[1]): sigma[1] = 0.4 * (newvoxelsize[1]/voxelsize[1])
        if(newvoxelsize[2] > voxelsize[2]): sigma[2] = 0.4 * (newvoxelsize[2]/voxelsize[2])
        
        if(sigma != [0, 0, 0]):
            print("Image is filtered before downsampling")
            mr = scipy.ndimage.gaussian_filter(mr, sigma)
        else:
            print("Image does not need filtering")
                
            
        xi = np.array(np.meshgrid(VoxelX_target, VoxelY_target, VoxelZ_target))
        xi = np.rollaxis(xi, 0, 4)
        xi = xi.reshape((xi.size // 3, 3))
        
        # get resized ct
        mr = scipy.interpolate.interpn((VoxelX_source,VoxelY_source,VoxelZ_source), mr, xi, method='linear', fill_value=0, bounds_error=False).reshape(target_shape).transpose(1,0,2)
        

    self.FrameOfReferenceUID = dcm.FrameOfReferenceUID
    self.ImagePositionPatient = [float(dcm.ImagePositionPatient[0]), float(dcm.ImagePositionPatient[1]), SliceLocation[0]]
    self.PixelSpacing = [float(dcm.PixelSpacing[0]), float(dcm.PixelSpacing[1]), MeanSliceDistance] if newvoxelsize is None else newvoxelsize
    self.GridSize = list(mr.shape)
    self.NumVoxels = self.GridSize[0] * self.GridSize[1] * self.GridSize[2]
    self.Image = mr
    self.SOPInstanceUIDs = SOPInstanceUIDs
    self.VoxelX = self.ImagePositionPatient[0] + np.arange(self.GridSize[0])*self.PixelSpacing[0]
    self.VoxelY = self.ImagePositionPatient[1] + np.arange(self.GridSize[1])*self.PixelSpacing[1]
    self.VoxelZ = self.ImagePositionPatient[2] + np.arange(self.GridSize[2])*self.PixelSpacing[2]
    self.isLoaded = 1