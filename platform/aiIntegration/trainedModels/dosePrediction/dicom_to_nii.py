import pydicom
import os
import numpy as np
import nibabel as nib
import scipy
from PIL import Image, ImageDraw


def convert_ct_dicom_to_nii(dir_dicom, dir_nii, outputname='ct.nii.gz', newvoxelsize = None):
    Patients = PatientList() # initialize list of patient data
    Patients.list_dicom_files(dir_dicom, 1) # search dicom files in the patient data folder, stores all files in the attributes (all CT images, dose file, struct file)
    patient = Patients.list[0]
    patient_name = patient.PatientInfo.PatientName
    patient.import_patient_data(newvoxelsize)
    CT = patient.CTimages[0]
    image_position_patient = CT.ImagePositionPatient
    voxelsize = np.array(CT.PixelSpacing)
    save_images(dst_dir=os.path.join(dir_nii), voxelsize=voxelsize, image_position_patient=image_position_patient, image=CT.Image, outputname=outputname)
    return CT

def convert_ct_struct_dicom_to_nii(dir_dicom, dir_nii, oar_list, tv_list, outputnameTV, outputnameOar, newvoxelsize = None):
    Patients = PatientList() # initialize list of patient data
    Patients.list_dicom_files(dir_dicom, 1) # search dicom files in the patient data folder, stores all files in the attributes (all CT images, dose file, struct file)
    patient = Patients.list[0]
    patient_name = patient.PatientInfo.PatientName
    channel_list = [*tv_list, *oar_list]
    patient.import_patient_data(newvoxelsize, channel_list)
    CT = patient.CTimages[0]
    Struct = patient.RTstructs_CT[0]
    
    image_position_patient = CT.ImagePositionPatient
    voxelsize = np.array(CT.PixelSpacing)
    
    save_images(dst_dir=dir_nii, voxelsize=voxelsize, image_position_patient=image_position_patient, image=CT.Image, struct=Struct, oar_list=oar_list, tv_list=tv_list, outputnameTV=outputnameTV, outputnameOar=outputnameOar)
    del Struct
    return CT
        

def save_images(dst_dir, voxelsize, image_position_patient, image, struct, oar_list, tv_list, outputnameTV, outputnameOar):
    
    # encode in nii  and save at dst_dir
    # IMPORTANT I NEED TO CONFIRM THE SIGNS OF THE ENTRIES IN THE AFFINE, 
    # ALTHOUGH MAYBE AT THE END THE IMPORTANCE IS HOW WE WILL USE THIS DATA .... 
    # also instead of changing field by field, the pixdim and affine can be encoded
    # using the set_sform method --> info here: https://nipy.org/nibabel/nifti_images.html
    tv = {}
    oar = {}
    for contour_id in range(struct.NumContours):
      contour_name = struct.Contours[contour_id].ROIName
      if contour_name in tv_list:
        tv[contour_name] = struct.Contours[contour_id].Mask
      elif contour_name in oar_list:
        oar[contour_name] = struct.Contours[contour_id].Mask
    # IMAGE (CT, MR ...)
    image_shape = image.shape
    # Separate Conversion from preprocessing
    # image = overwrite_ct_threshold(image)
    image_nii = nib.Nifti1Image(image, affine=np.eye(4)) # for Nifti1 header, change for a Nifti2 type of header
    # Update header fields
    image_nii = set_header_info(image_nii, voxelsize, image_position_patient)
    # Save  nii 
    nib.save(image_nii, os.path.join(dst_dir,'ct.nii.gz'))
    # TV
    tv_npy = get_tv(tv, image_nii.header, tv_list)
    print("np.unique(tv_npy)",np.unique(tv_npy))
    print("tv.keys()",tv.keys())
    tv_nii = nib.Nifti1Image(tv_npy, affine=np.eye(4)) # for Nifti1 header, change for a Nifti2 type of header
    # Update header fields
    tv_nii = set_header_info(tv_nii, voxelsize, image_position_patient)
    nib.save(tv_nii, os.path.join(dst_dir, outputnameTV))
    
    # OARs
    oar_npy = get_oars(oar, image_nii.header, oar_list)
    print("np.unique(oar_npy)",np.unique(oar_npy))
    print("oar.keys()",oar.keys())
    oar_nii = nib.Nifti1Image(oar_npy, affine=np.eye(4)) # for Nifti1 header, change for a Nifti2 type of header
    # Update header fields
    oar_nii = set_header_info(oar_nii, voxelsize, image_position_patient,contours_exist=[1]*len(oar_list))
    nib.save(oar_nii, os.path.join(dst_dir, outputnameOar))

def get_tv(roi, nib_header, tv_names):
    tv = np.zeros(nib_header['dim'][1:4])
    roi_names = list(roi.keys())
    if tv_names is not None:
        for c in tv_names:
            if c in roi_names:
                tv[roi[c]>0]=int(c.split('_')[1])/100
    return tv

def get_oars(roi, nib_header, oar_names):
    oars = np.zeros(nib_header['dim'][1:4])
    
    roi_names = list(roi.keys())
    contoursexist = []
    
    if oar_names is not None:
        for i in range(len(oar_names)):
            if oar_names[i] in roi_names:
                if roi[oar_names[i]] is not None:
                    contoursexist.append(1)
                    oars += (2**i)*roi[oar_names[i]]
                else:
                    contoursexist.append(0) 
    return oars       

def overwrite_ct_threshold(ct_image, body, artefact = None, contrast = None):
    # Change the HU out of the body to air: -1000    
    ct_image[body==0]=-1000
    if artefact is not None:
        # Change the HU to muscle: 14
        ct_image[artefact==1]=14
    if contrast is not None:
        # Change the HU to water: 0 Houndsfield Unit: CT unit
        ct_image[contrast==1]=0
    # Threshold above 1560HU
    ct_image[ct_image > 1560] = 1560
    return ct_image
        
def set_header_info(nii_file, voxelsize, image_position_patient, contours_exist = None):
    nii_file.header['pixdim'][1] = voxelsize[0]
    nii_file.header['pixdim'][2] = voxelsize[1]
    nii_file.header['pixdim'][3] = voxelsize[2]
    
    #affine - voxelsize
    nii_file.affine[0][0] = voxelsize[0]
    nii_file.affine[1][1] = voxelsize[1]
    nii_file.affine[2][2] = voxelsize[2]
    #affine - imagecorner
    nii_file.affine[0][3] = image_position_patient[0]
    nii_file.affine[1][3] = image_position_patient[1]
    nii_file.affine[2][3] = image_position_patient[2]
    if contours_exist is not None:
        print('len(contours_exist) in set_header_info',len(contours_exist))
        nii_file.header.extensions.append(nib.nifti1.Nifti1Extension(0, bytearray(contours_exist)))
    return nii_file   

class PatientList:

  def __init__(self):
    self.list = []
    
    
  
  def find_CT_image(self, display_id):
    count = -1
    for patient_id in range(len(self.list)):
      for ct_id in range(len(self.list[patient_id].CTimages)):
        if(self.list[patient_id].CTimages[ct_id].isLoaded == 1): count += 1
        if(count == display_id): break
      if(count == display_id): break
      
    return patient_id, ct_id
    
    
  
  def find_dose_image(self, display_id):
    count = -1
    for patient_id in range(len(self.list)):
      for dose_id in range(len(self.list[patient_id].RTdoses)):
        if(self.list[patient_id].RTdoses[dose_id].isLoaded == 1): count += 1
        if(count == display_id): break
      if(count == display_id): break
      
    return patient_id, dose_id
      
    
  
  def find_contour(self, ROIName):
    for patient_id in range(len(self.list)):
      for struct_id in range(len(self.list[patient_id].RTstructs)):
        if(self.list[patient_id].RTstructs[struct_id].isLoaded == 1):
          for contour_id in range(len(self.list[patient_id].RTstructs[struct_id].Contours)):
            if(self.list[patient_id].RTstructs[struct_id].Contours[contour_id].ROIName == ROIName):
              return patient_id, struct_id, contour_id
    
    
    
  def list_dicom_files(self, folder_path, recursive):
    file_list = os.listdir(folder_path)
    #print("len file_list", len(file_list), "folderpath",folder_path)
    for file_name in file_list:
      file_path = os.path.join(folder_path, file_name)
      
      # folders
      if os.path.isdir(file_path):
        if recursive == True:
          subfolder_list = self.list_dicom_files(file_path, True)
          #join_patient_lists(Patients, subfolder_list)
          
      # files
      elif os.path.isfile(file_path):
      
        try:
          dcm = pydicom.dcmread(file_path)
        except:
          print("Invalid Dicom file: " + file_path)
          continue
        
        patient_id = next((x for x, val in enumerate(self.list) if val.PatientInfo.PatientID == dcm.PatientID), -1)

        
        if patient_id == -1:
          Patient = PatientData()
          Patient.PatientInfo.PatientID = dcm.PatientID
          Patient.PatientInfo.PatientName = str(dcm.PatientName)
          Patient.PatientInfo.PatientBirthDate = dcm.PatientBirthDate
          Patient.PatientInfo.PatientSex = dcm.PatientSex
          self.list.append(Patient)
          patient_id = len(self.list) - 1

        # Dicom CT
        if dcm.SOPClassUID == "1.2.840.10008.5.1.4.1.1.2":
          ct_id = next((x for x, val in enumerate(self.list[patient_id].CTimages) if val.SeriesInstanceUID == dcm.SeriesInstanceUID), -1)
          if ct_id == -1:
            CT = CTimage()
            CT.SeriesInstanceUID = dcm.SeriesInstanceUID
            CT.SOPClassUID == "1.2.840.10008.5.1.4.1.1.2"
            CT.PatientInfo = self.list[patient_id].PatientInfo
            CT.StudyInfo = StudyInfo()
            CT.StudyInfo.StudyInstanceUID = dcm.StudyInstanceUID
            CT.StudyInfo.StudyID = dcm.StudyID
            CT.StudyInfo.StudyDate = dcm.StudyDate
            CT.StudyInfo.StudyTime = dcm.StudyTime
            if(hasattr(dcm, 'SeriesDescription') and dcm.SeriesDescription != ""): CT.ImgName = dcm.SeriesDescription
            else: CT.ImgName = dcm.SeriesInstanceUID
            self.list[patient_id].CTimages.append(CT)
            ct_id = len(self.list[patient_id].CTimages) - 1

          self.list[patient_id].CTimages[ct_id].DcmFiles.append(file_path)
        # Dicom struct
        elif dcm.SOPClassUID == "1.2.840.10008.5.1.4.1.1.481.3":
          #struct_id = next((x for x, val in enumerate(self.list[patient_id].RTstructs_CT) if val.SeriesInstanceUID == dcm.SeriesInstanceUID), -1)
          #if struct_id == -1:
          struct = RTstruct()
          struct.SeriesInstanceUID = dcm.SeriesInstanceUID
          struct.PatientInfo = self.list[patient_id].PatientInfo
          struct.StudyInfo = StudyInfo()
          struct.StudyInfo.StudyInstanceUID = dcm.StudyInstanceUID
          struct.StudyInfo.StudyID = dcm.StudyID
          struct.StudyInfo.StudyDate = dcm.StudyDate
          struct.StudyInfo.StudyTime = dcm.StudyTime
          struct.DcmFile = file_path
          
          if dcm.ROIContourSequence[0].ContourSequence[0].ContourImageSequence[0].ReferencedSOPClassUID=="1.2.840.10008.5.1.4.1.1.2":
            self.list[patient_id].RTstructs_CT.append(struct)
          elif dcm.ROIContourSequence[0].ContourSequence[0].ContourImageSequence[0].ReferencedSOPClassUID=="1.2.840.10008.5.1.4.1.1.4":
            self.list[patient_id].RTstructs_MR.append(struct)
          else: 
            print("Missing neccessary dicom tag: ReferencedSOPClassUID")
        
        else:
          print("Unknown SOPClassUID " + dcm.SOPClassUID + " for file " + file_path)

      # other
      else:
        print("Unknown file type " + file_path)


  def print_patient_list(self):
    print("")
    for patient in self.list:
      patient.print_patient_info()

    print("")



class PatientData:

  def __init__(self):
    self.PatientInfo = PatientInfo()
    self.CTimages = []
    self.RTstructs_CT = []
  def print_patient_info(self, prefix=""):
    print("")
    print(prefix + "PatientName: " + self.PatientInfo.PatientName)
    print(prefix+ "PatientID: " + self.PatientInfo.PatientID)
    
    for ct in self.CTimages:
      print("")
      ct.print_CT_info(prefix + "   ")
    

  def import_patient_data(self,newvoxelsize=None, struct_list=None):
    # import CT images
    for i,ct in enumerate(self.CTimages):
      if(ct.isLoaded == 1): continue
      ct.import_Dicom_CT()
    # import RTstructs linked to CT
    for i, struct in enumerate(self.RTstructs_CT):
      struct.import_Dicom_struct(self.CTimages[i], struct_list = struct_list) # to be improved: user select CT image
    # Resample CT images
    for i,ct in enumerate(self.CTimages):
      ct.resample_CT(newvoxelsize)
    # Resample RTstructs linked to CT images
    for i, struct in enumerate(self.RTstructs_CT):
      struct.resample_struct(newvoxelsize) # to be improved: user select CT image
    


class PatientInfo:

  def __init__(self):
    self.PatientID = ''
    self.PatientName = ''
    self.PatientBirthDate = ''
    self.PatientSex = ''


class StudyInfo:

  def __init__(self):
    self.StudyInstanceUID = ''
    self.StudyID = ''
    self.StudyDate = ''
    self.StudyTime = ''

class CTimage:

  def __init__(self):
    self.SeriesInstanceUID = ""
    self.PatientInfo = {}
    self.StudyInfo = {}
    self.FrameOfReferenceUID = ""
    self.ImgName = ""
    self.SOPClassUID = ""
    self.DcmFiles = []
    self.isLoaded = 0
    
    
    
  def print_CT_info(self, prefix=""):
    print(prefix + "CT series: " + self.SeriesInstanceUID)
    for ct_slice in self.DcmFiles:
      print(prefix + "   " + ct_slice)
      
      
  def resample_CT(self, newvoxelsize):
    ct = self.Image
    # Rescaling to the newvoxelsize if given in parameter
    if newvoxelsize is not None:
      source_shape = self.GridSize
      voxelsize = self.PixelSpacing
      #print("self.ImagePositionPatient",self.ImagePositionPatient, "source_shape",source_shape,"voxelsize",voxelsize)
      VoxelX_source = self.ImagePositionPatient[0] + np.arange(source_shape[0])*voxelsize[0]
      VoxelY_source = self.ImagePositionPatient[1] + np.arange(source_shape[1])*voxelsize[1]
      VoxelZ_source = self.ImagePositionPatient[2] + np.arange(source_shape[2])*voxelsize[2]

      target_shape = np.ceil(np.array(source_shape).astype(float)*np.array(voxelsize).astype(float)/newvoxelsize).astype(int)
      VoxelX_target = self.ImagePositionPatient[0] + np.arange(target_shape[0])*newvoxelsize[0]
      VoxelY_target = self.ImagePositionPatient[1] + np.arange(target_shape[1])*newvoxelsize[1]
      VoxelZ_target = self.ImagePositionPatient[2] + np.arange(target_shape[2])*newvoxelsize[2]
      #print("source_shape",source_shape,"target_shape",target_shape)
      if(all(source_shape == target_shape) and np.linalg.norm(np.subtract(voxelsize, newvoxelsize) < 0.001)):
        print("Image does not need filtering")
      else:
        # anti-aliasing filter
        sigma = [0, 0, 0]
        if(newvoxelsize[0] > voxelsize[0]): sigma[0] = 0.4 * (newvoxelsize[0]/voxelsize[0])
        if(newvoxelsize[1] > voxelsize[1]): sigma[1] = 0.4 * (newvoxelsize[1]/voxelsize[1])
        if(newvoxelsize[2] > voxelsize[2]): sigma[2] = 0.4 * (newvoxelsize[2]/voxelsize[2])
        
        if(sigma != [0, 0, 0]):
            print("Image is filtered before downsampling")
            ct = scipy.ndimage.gaussian_filter(ct, sigma)
        
                
            
        xi = np.array(np.meshgrid(VoxelX_target, VoxelY_target, VoxelZ_target))
        xi = np.rollaxis(xi, 0, 4)
        xi = xi.reshape((xi.size // 3, 3))
        
        # get resized ct
        ct = scipy.interpolate.interpn((VoxelX_source,VoxelY_source,VoxelZ_source), ct, xi, method='linear', fill_value=-1000, bounds_error=False).reshape(target_shape).transpose(1,0,2)
    
      self.PixelSpacing = newvoxelsize
    self.GridSize = list(ct.shape) 
    self.NumVoxels = self.GridSize[0] * self.GridSize[1] * self.GridSize[2]
    self.Image = ct
    #print("self.ImagePositionPatient",self.ImagePositionPatient, "self.GridSize[0]",self.GridSize[0],"self.PixelSpacing",self.PixelSpacing)
      
    self.VoxelX = self.ImagePositionPatient[0] + np.arange(self.GridSize[0])*self.PixelSpacing[0]
    self.VoxelY = self.ImagePositionPatient[1] + np.arange(self.GridSize[1])*self.PixelSpacing[1]
    self.VoxelZ = self.ImagePositionPatient[2] + np.arange(self.GridSize[2])*self.PixelSpacing[2]
    self.isLoaded = 1

  def import_Dicom_CT(self):
    
    if(self.isLoaded == 1):
      print("Warning: CT serries " + self.SeriesInstanceUID + " is already loaded")
      return
  
    images = []
    SOPInstanceUIDs = []
    SliceLocation = np.zeros(len(self.DcmFiles), dtype='float')

    for i in range(len(self.DcmFiles)):
      file_path = self.DcmFiles[i]
      dcm = pydicom.dcmread(file_path)

      if(hasattr(dcm, 'SliceLocation') and abs(dcm.SliceLocation - dcm.ImagePositionPatient[2]) > 0.001):
        print("WARNING: SliceLocation (" + str(dcm.SliceLocation) + ") is different than ImagePositionPatient[2] (" + str(dcm.ImagePositionPatient[2]) + ") for " + file_path)

      SliceLocation[i] = float(dcm.ImagePositionPatient[2])
      images.append(dcm.pixel_array * dcm.RescaleSlope + dcm.RescaleIntercept)
      SOPInstanceUIDs.append(dcm.SOPInstanceUID)

    # sort slices according to their location in order to reconstruct the 3d image
    sort_index = np.argsort(SliceLocation)
    SliceLocation = SliceLocation[sort_index]
    SOPInstanceUIDs = [SOPInstanceUIDs[n] for n in sort_index]
    images = [images[n] for n in sort_index]
    ct = np.dstack(images).astype("float32")

    if ct.shape[0:2] != (dcm.Rows, dcm.Columns):
      print("WARNING: GridSize " + str(ct.shape[0:2]) + " different from Dicom Rows (" + str(dcm.Rows) + ") and Columns (" + str(dcm.Columns) + ")")

    MeanSliceDistance = (SliceLocation[-1] - SliceLocation[0]) / (len(images)-1)
    if(abs(MeanSliceDistance - dcm.SliceThickness) > 0.001):
      print("WARNING: MeanSliceDistance (" + str(MeanSliceDistance) + ") is different from SliceThickness (" + str(dcm.SliceThickness) + ")")

    self.SOPClassUID = dcm.SOPClassUID
    self.FrameOfReferenceUID = dcm.FrameOfReferenceUID
    self.ImagePositionPatient = [float(dcm.ImagePositionPatient[0]), float(dcm.ImagePositionPatient[1]), SliceLocation[0]]
    self.PixelSpacing = [float(dcm.PixelSpacing[0]), float(dcm.PixelSpacing[1]), MeanSliceDistance]
    self.GridSize = list(ct.shape) 
    self.NumVoxels = self.GridSize[0] * self.GridSize[1] * self.GridSize[2]
    self.Image = ct
    self.SOPInstanceUIDs = SOPInstanceUIDs
    self.VoxelX = self.ImagePositionPatient[0] + np.arange(self.GridSize[0])*self.PixelSpacing[0]
    self.VoxelY = self.ImagePositionPatient[1] + np.arange(self.GridSize[1])*self.PixelSpacing[1]
    self.VoxelZ = self.ImagePositionPatient[2] + np.arange(self.GridSize[2])*self.PixelSpacing[2]
    self.isLoaded = 1

def Taubin_smoothing(contour):
    """ Here, we do smoothing in 2D contours!
        Parameters:
            a Nx2 numpy array containing the contour to smooth
        Returns:
            a Nx2 numpy array containing the smoothed contour """
    smoothingloops = 5
    smoothed = [np.empty_like(contour) for i in range(smoothingloops+1)]
    smoothed[0] = contour
    for i in range(smoothingloops):
        # loop over all elements in the contour
        for vertex_i in range(smoothed[0].shape[0]):
            if vertex_i == 0:
                vertex_prev = smoothed[i].shape[0]-1
                vertex_next = vertex_i+1
            elif vertex_i == smoothed[i].shape[0]-1:
                vertex_prev = vertex_i-1
                vertex_next = 0
            else:
                vertex_prev = vertex_i -1
                vertex_next = vertex_i +1
            neighbours_x = np.array([smoothed[i][vertex_prev,0], smoothed[i][vertex_next,0]])
            neighbours_y = np.array([smoothed[i][vertex_prev,1], smoothed[i][vertex_next,1]])
            smoothed[i+1][vertex_i,0] = smoothed[i][vertex_i,0] - 0.3*(smoothed[i][vertex_i,0] - np.mean(neighbours_x))
            smoothed[i+1][vertex_i,1] = smoothed[i][vertex_i,1] - 0.3*(smoothed[i][vertex_i,1] - np.mean(neighbours_y))

    return np.round(smoothed[smoothingloops],3)



class RTstruct:

  def __init__(self):
    self.SeriesInstanceUID = ""
    self.PatientInfo = {}
    self.StudyInfo = {}
    self.CT_SeriesInstanceUID = ""
    self.DcmFile = ""
    self.isLoaded = 0
    self.Contours = []
    self.NumContours = 0
    
    
  def print_struct_info(self, prefix=""):
    print(prefix + "Struct: " + self.SeriesInstanceUID)
    print(prefix + "   " + self.DcmFile)
    
    
  def print_ROINames(self):
    print("RT Struct UID: " + self.SeriesInstanceUID)
    count = -1
    for contour in self.Contours:
      count += 1
      print('  [' + str(count) + ']  ' + contour.ROIName)
    
  def resample_struct(self, newvoxelsize):
    # Rescaling to the newvoxelsize if given in parameter
    if newvoxelsize is not None: 
      for i, Contour in enumerate(self.Contours):
        source_shape = Contour.Mask_GridSize
        voxelsize = Contour.Mask_PixelSpacing
        VoxelX_source = Contour.Mask_Offset[0] + np.arange(source_shape[0])*voxelsize[0]
        VoxelY_source = Contour.Mask_Offset[1] + np.arange(source_shape[1])*voxelsize[1]
        VoxelZ_source = Contour.Mask_Offset[2] + np.arange(source_shape[2])*voxelsize[2]

        target_shape = np.ceil(np.array(source_shape).astype(float)*np.array(voxelsize).astype(float)/newvoxelsize).astype(int)
        VoxelX_target = Contour.Mask_Offset[0] + np.arange(target_shape[0])*newvoxelsize[0]
        VoxelY_target = Contour.Mask_Offset[1] + np.arange(target_shape[1])*newvoxelsize[1]
        VoxelZ_target = Contour.Mask_Offset[2] + np.arange(target_shape[2])*newvoxelsize[2]

        contour = Contour.Mask
        
        if(all(source_shape == target_shape) and np.linalg.norm(np.subtract(voxelsize, newvoxelsize) < 0.001)):
          print("! Image does not need filtering")
        else:
          # anti-aliasing filter
          sigma = [0, 0, 0]
          if(newvoxelsize[0] > voxelsize[0]): sigma[0] = 0.4 * (newvoxelsize[0]/voxelsize[0])
          if(newvoxelsize[1] > voxelsize[1]): sigma[1] = 0.4 * (newvoxelsize[1]/voxelsize[1])
          if(newvoxelsize[2] > voxelsize[2]): sigma[2] = 0.4 * (newvoxelsize[2]/voxelsize[2])
          
          if(sigma != [0, 0, 0]):
              contour = scipy.ndimage.gaussian_filter(contour.astype(float), sigma)
              #come back to binary
              contour[np.where(contour>=0.5)] = 1
              contour[np.where(contour<0.5)] = 0
              
          xi = np.array(np.meshgrid(VoxelX_target, VoxelY_target, VoxelZ_target))
          xi = np.rollaxis(xi, 0, 4)
          xi = xi.reshape((xi.size // 3, 3))
          
          # get resized ct
          contour = scipy.interpolate.interpn((VoxelX_source,VoxelY_source,VoxelZ_source), contour, xi, method='nearest', fill_value=0, bounds_error=False).astype(bool).reshape(target_shape).transpose(1,0,2)
        Contour.Mask_PixelSpacing = newvoxelsize
        Contour.Mask_GridSize = list(contour.shape) 
        Contour.NumVoxels = Contour.Mask_GridSize[0] * Contour.Mask_GridSize[1] * Contour.Mask_GridSize[2]
        Contour.Mask = contour
        self.Contours[i]=Contour
        
  
  def import_Dicom_struct(self, CT, struct_list = None):
    if(self.isLoaded == 1):
      print("Warning: RTstruct " + self.SeriesInstanceUID + " is already loaded")
      return 
    dcm = pydicom.dcmread(self.DcmFile)
    
    self.CT_SeriesInstanceUID = CT.SeriesInstanceUID
    print("struct_list",struct_list)
    for dcm_struct in dcm.StructureSetROISequence: 
      
      print("dcm_struct.ROIName",dcm_struct.ROIName)
      if struct_list is not None and dcm_struct.ROIName not in struct_list:
        continue 
      print("in if")
      ReferencedROI_id = next((x for x, val in enumerate(dcm.ROIContourSequence) if val.ReferencedROINumber == dcm_struct.ROINumber), -1)
      dcm_contour = dcm.ROIContourSequence[ReferencedROI_id]
    
      Contour = ROIcontour()
      Contour.SeriesInstanceUID = self.SeriesInstanceUID
      Contour.ROIName = dcm_struct.ROIName
      Contour.ROIDisplayColor = dcm_contour.ROIDisplayColor
    
      #print("Import contour " + str(len(self.Contours)) + ": " + Contour.ROIName)
    
      Contour.Mask = np.zeros((CT.GridSize[0], CT.GridSize[1], CT.GridSize[2]), dtype=np.bool_)
      Contour.Mask_GridSize = CT.GridSize
      Contour.Mask_PixelSpacing = CT.PixelSpacing
      Contour.Mask_Offset = CT.ImagePositionPatient
      Contour.Mask_NumVoxels = CT.NumVoxels   
      Contour.ContourMask = np.zeros((CT.GridSize[0], CT.GridSize[1], CT.GridSize[2]), dtype=np.bool_)
      
      SOPInstanceUID_match = 1
      
      if not hasattr(dcm_contour, 'ContourSequence'):
          print("This structure has no attribute ContourSequence. Skipping ...")
          continue

      for dcm_slice in dcm_contour.ContourSequence:
        Slice = {}
      
        # list of Dicom coordinates
        Slice["XY_dcm"] = list(zip( np.array(dcm_slice.ContourData[0::3]), np.array(dcm_slice.ContourData[1::3]) ))
        Slice["Z_dcm"] = float(dcm_slice.ContourData[2])
      
        # list of coordinates in the image frame
        Slice["XY_img"] = list(zip( ((np.array(dcm_slice.ContourData[0::3]) - CT.ImagePositionPatient[0]) / CT.PixelSpacing[0]), ((np.array(dcm_slice.ContourData[1::3]) - CT.ImagePositionPatient[1]) / CT.PixelSpacing[1]) ))
        Slice["Z_img"] = (Slice["Z_dcm"] - CT.ImagePositionPatient[2]) / CT.PixelSpacing[2]
        Slice["Slice_id"] = int(round(Slice["Z_img"]))
      
        # convert polygon to mask (based on matplotlib - slow)
        #x, y = np.meshgrid(np.arange(CT.GridSize[0]), np.arange(CT.GridSize[1]))
        #points = np.transpose((x.ravel(), y.ravel()))
        #path = Path(Slice["XY_img"])
        #mask = path.contains_points(points)
        #mask = mask.reshape((CT.GridSize[0], CT.GridSize[1]))
      
        # convert polygon to mask (based on PIL - fast)
        img = Image.new('L', (CT.GridSize[0], CT.GridSize[1]), 0)
        if(len(Slice["XY_img"]) > 1): ImageDraw.Draw(img).polygon(Slice["XY_img"], outline=1, fill=1)
        mask = np.array(img)
        Contour.Mask[:,:,Slice["Slice_id"]] = np.logical_or(Contour.Mask[:,:,Slice["Slice_id"]], mask)
        
        # do the same, but only keep contour in the mask
        img = Image.new('L', (CT.GridSize[0], CT.GridSize[1]), 0)
        if(len(Slice["XY_img"]) > 1): ImageDraw.Draw(img).polygon(Slice["XY_img"], outline=1, fill=0)
        mask = np.array(img)
        Contour.ContourMask[:,:,Slice["Slice_id"]] = np.logical_or(Contour.ContourMask[:,:,Slice["Slice_id"]], mask)
            
        Contour.ContourSequence.append(Slice)
      
        # check if the contour sequence is imported on the correct CT slice:
        if(hasattr(dcm_slice, 'ContourImageSequence') and CT.SOPInstanceUIDs[Slice["Slice_id"]] != dcm_slice.ContourImageSequence[0].ReferencedSOPInstanceUID):
          SOPInstanceUID_match = 0
      
      if SOPInstanceUID_match != 1:
        print("WARNING: some SOPInstanceUIDs don't match during importation of " + Contour.ROIName + " contour on CT image")
      
      self.Contours.append(Contour)
      self.NumContours += 1
    #print("self.NumContours",self.NumContours, len(self.Contours))
    self.isLoaded = 1   

class ROIcontour:

    def __init__(self):
      self.SeriesInstanceUID = ""
      self.ROIName = ""
      self.ContourSequence = []
      
    def getROIContour(self): # this is from new version of OpenTPS, I(ana) have adapted it to work with old version of self.Contours[i].Mask
    
        try:
            from skimage.measure import label, find_contours
            from skimage.segmentation import find_boundaries
        except:
            print('Module skimage (scikit-image) not installed, ROIMask cannot be converted to ROIContour')
            return 0
    
        polygonMeshList = []
        for zSlice in range(self.Mask.shape[2]):
    
            labeledImg, numberOfLabel = label(self.Mask[:, :, zSlice], return_num=True)
    
            for i in range(1, numberOfLabel + 1):
    
                singleLabelImg = labeledImg == i
                contours = find_contours(singleLabelImg.astype(np.uint8), level=0.6)
    
                if len(contours) > 0:
    
                    if len(contours) == 2:
    
                        ## use a different threshold in the case of an interior contour
                        contours2 = find_contours(singleLabelImg.astype(np.uint8), level=0.4)
    
                        interiorContour = contours2[1]
                        polygonMesh = []
                        for point in interiorContour:
    
                            #xCoord = np.round(point[1]) * self.Mask_PixelSpacing[1] + self.Mask_Offset[1] # original Damien in OpenTPS
                            #yCoord = np.round(point[0]) * self.Mask_PixelSpacing[0] + self.Mask_Offset[0] # original Damien in OpenTPS
                            xCoord = np.round(point[1]) * self.Mask_PixelSpacing[0] + self.Mask_Offset[0] #AB
                            yCoord = np.round(point[0]) * self.Mask_PixelSpacing[1] + self.Mask_Offset[1] #AB
                            zCoord = zSlice * self.Mask_PixelSpacing[2] + self.Mask_Offset[2]
    
                            #polygonMesh.append(yCoord) # original Damien in OpenTPS
                            #polygonMesh.append(xCoord) # original Damien in OpenTPS
                            polygonMesh.append(xCoord) # AB
                            polygonMesh.append(yCoord) # AB
                            polygonMesh.append(zCoord)
    
                        polygonMeshList.append(polygonMesh)
    
                    contour = contours[0]
    
                    polygonMesh = []
                    for point in contour:
    
                        #xCoord = np.round(point[1]) * self.Mask_PixelSpacing[1] + self.Mask_Offset[1] # original Damien in OpenTPS
                        #yCoord = np.round(point[0]) * self.Mask_PixelSpacing[0] + self.Mask_Offset[0] # original Damien in OpenTPS
                        xCoord = np.round(point[1]) * self.Mask_PixelSpacing[0] + self.Mask_Offset[0] #AB
                        yCoord = np.round(point[0]) * self.Mask_PixelSpacing[1] + self.Mask_Offset[1] #AB
                        zCoord = zSlice * self.Mask_PixelSpacing[2] + self.Mask_Offset[2]
    
                        polygonMesh.append(xCoord) # AB
                        polygonMesh.append(yCoord) # AB
                        #polygonMesh.append(yCoord) # original Damien in OpenTPS
                        #polygonMesh.append(xCoord) # original Damien in OpenTPS
                        polygonMesh.append(zCoord)
    
                    polygonMeshList.append(polygonMesh)
    
        ## I (ana) will comment this part since I will not use the class ROIContour for simplicity ###
        #from opentps.core.data._roiContour import ROIContour  ## this is done here to avoir circular imports issue
        #contour = ROIContour(name=self.ROIName, displayColor=self.ROIDisplayColor)
        #contour.polygonMesh = polygonMeshList
    
        #return contour
        
        # instead returning the polygonMeshList directly
        return polygonMeshList
    

    


    
