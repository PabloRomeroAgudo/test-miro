from api_server.api import bp
from api_server.api.errors import bad_request, error_response
from api_server.pythonUtils.utils import createTemporaryDirectory
from api_server.api.ospipen import pipe_no_wait
from flask import jsonify, request
import os
import os.path
import json
import sys, stat
import subprocess
import shutil
import time

runningprocess = None

def setInterpreterPackage(version):
    if (version =='py368'):
        return 'prediction-3.6.8'
    elif (version == 'py3109'):
        return 'prediction-3.10.9'
    else:
        return ''
    
# Runing of prediction of the AI model
# http://localhost:3001/api/runprediction
@bp.route('/runprediction', methods=['POST'])
def runPrediction():
    global runningprocess
    # Check if there is already a prediction running
    if (runningprocess is not None) and (runningprocess.poll() is None):
        return error_response(404, "There is already a prediction running")
    else:
        runningprocess = None
    
    # Parse request
    data = request.get_json() or {}
    if ('category' not in data) or data['category'] == "" or data['category'] is None:
        return bad_request("Missing the category: Contour prediction or Dose prediction")
    if ('interpreter' not in data) or data["interpreter"] == "" or data["interpreter"] is None:
        return bad_request("No interpreter selected")
    if ('path' not in data) or data["path"] == "" or data["path"] is None:
        return bad_request("No predict source code path")
    if ('temporaryDirectoryPath' not in data) or data["temporaryDirectoryPath"] == "" or data["temporaryDirectoryPath"] is None:
        return bad_request("No temporary directory path")
    if ('patientName' not in data) or data["patientName"] == "" or data["patientName"] is None:
        return bad_request("Missing patient data: patientName")

    if ((('ctSeriesInstanceUID' not in data) or (data["ctSeriesInstanceUID"] == "") or (data["ctSeriesInstanceUID"] is None))):
        print("Missing patient data: There are CT image data set")

    if ('category' in data) and data['category'] == 'doseprediction':
        if ('oarList' not in data) or (data["oarList"] == "") or (data["oarList"] is None):
            return bad_request("Missing oar list")    
        if ('tvList' not in data) or (data["tvList"] == "") or (data["tvList"] is None):
            return bad_request("Missing tv list")
    # else:
    #     if ('rtStructSeriesInstanceUID' not in data) or (data["rtStructSeriesInstanceUID"] == "") or (data["rtStructSeriesInstanceUID"] is None):
    #         return bad_request("Missing patient data: RTSTRUCT SeriesInstanceUID")
    
    path = data["path"]
    basename = os.path.basename(path)
    dirname = os.path.dirname(path)
    if basename == "" or os.path.isdir(path):
        return error_response(404, "Path is not valid")
    tempPath = os.path.join(os.path.abspath(data["temporaryDirectoryPath"]), "parrot_prediction")
    patientname = data["patientName"]
    ctSeriesInstanceUID = ""
    rtStructSeriesInstanceUID = ""
    regSeriesInstanceUID = ""
    oarList = []
    tvList = []
    if ('ctSeriesInstanceUID' in data) and data["ctSeriesInstanceUID"] != "" and not data["ctSeriesInstanceUID"] is None:
        ctSeriesInstanceUID = data["ctSeriesInstanceUID"]
    if ('rtStructSeriesInstanceUID' in data) and data['rtStructSeriesInstanceUID'] != "" and not data['rtStructSeriesInstanceUID'] is None:
        rtStructSeriesInstanceUID = data["rtStructSeriesInstanceUID"]
    if ('regSeriesInstanceUID' in data) and data['regSeriesInstanceUID'] != "" and not data['regSeriesInstanceUID'] is None:
        regSeriesInstanceUID = data["regSeriesInstanceUID"]
    if ('category' in data) and data['category'].lower() == 'doseprediction':
        oarList = data["oarList"]   
        tvList = data["tvList"]
    
    if (data["interpreter"]=="local"):            
        runInterpreter  = "python"
        interpreterPath = os.getenv("LOCALPYTHONPATH")
    else:
        runInterpreter = setInterpreterPackage(data["interpreter"])
        interpreterPath = os.path.join(os.getcwd(), 'python_environments', runInterpreter, 'python')
    
    # Start process
    try:
        # create temporary directory
        shutil.rmtree(tempPath, ignore_errors=True)
        createTemporaryDirectory(tempPath)
            
        # Set environment
        python_dir = os.path.dirname(interpreterPath)
        my_env = os.environ.copy()        
        my_env["PATH"] = python_dir + os.pathsep + my_env["PATH"]
        
        # run process
        runningprocess = subprocess.Popen(
            [interpreterPath, "-u", basename, tempPath, patientname, ctSeriesInstanceUID, rtStructSeriesInstanceUID, regSeriesInstanceUID, runInterpreter, json.dumps(oarList), json.dumps(tvList)], cwd=dirname, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True, shell=False, env=my_env)
        if sys.platform=="win32":
            pipe_no_wait(runningprocess.stdout.fileno())
        
        response = jsonify({
            "message": "process started"
        })
        response.status_code = 200
        return response
    except Exception as e:
        return error_response(404, "Cannot run the prediction:" + str(e))

# Stop the runing prediction
# http://localhost:3001/api/stopprediction


@bp.route('/stopprediction', methods=['POST'])
def stopPrediction():
    global runningprocess

    # Stop process
    if runningprocess is not None:
        if sys.platform=="win32":
            subprocess.call(['taskkill', '/F', '/T', '/PID', str(runningprocess.pid)])
        else:
            runningprocess.terminate()
        runningprocess = None

    response = jsonify({
        "message": "process stopped",
    })
    response.status_code = 200
    return response

# Output the prediction result
# http://localhost:3001/api/outprediction


@bp.route('/outprediction', methods=['POST'])
def getPredictionOutput():
    global runningprocess

    # Process running?
    if runningprocess is not None:
        # Read the output
        data = ""
        try:
            while True:
                line = runningprocess.stdout.readline()
                if not line:
                    break
                data = data + line
        except IOError as e:
           pass 
    else:
        data = ""

    if (runningprocess is not None) and (runningprocess.poll() == None):
        message = "process running"
        isRunning = True
    else:
        message = "process stopped"
        isRunning = False

    response = jsonify({
        "message": message, "output": data, "isRunning": isRunning
    })
    response.status_code = 200
    return response



# POST
#   {
#       path: "a/b/c"   or   ["a","b","c"]
#   }
# Result:
#   {
#       path: "a/b/c",
#       content: Status message
#   }
# Error: 404 if path does not exist or if path is a directory or file can not be modified
@bp.route('/savecode', methods=['POST'])
def savePredictionCode():
    # Parse request
    data = request.get_json() or {}
    if ('path' not in data) or data["path"] == "" or data["path"] is None:
        return bad_request("No predict source code path")
        
    if ('sourceCode' not in data) or data["sourceCode"] == "" or data["sourceCode"] is None:
        return bad_request("No predict python source code")
    
    filepath = data["path"]
    basename = os.path.basename(filepath)
    dirname = os.path.dirname(filepath)
            
    if basename == "" or os.path.isdir(filepath):
        return error_response(404, "Path is not valid")
        
    backupfilepath = filepath+".bak"
    backupfilepath2 = filepath+".bak2"
    
    try: 
        # make backup of old prediction code.
        if os.path.isfile(filepath):
            if os.path.isfile(backupfilepath):
                os.replace(backupfilepath, backupfilepath2)
            os.replace(filepath, backupfilepath)
            
        # write model info
        sourceCode = data['sourceCode']
        
        with open(filepath, "w") as outfile:
            outfile.write(sourceCode)
            # json.dump(sourceCode, outfile)
            
            # delete old backup
            if os.path.isfile(backupfilepath2):
                os.remove(backupfilepath2)
                
            response = jsonify({
                "message": 'Saving AI model predection code was successful'
            })
            response.status_code = 200
            return response
    except Exception as e:
        return error_response(500, "Storing the AI model prediction code failed: " + str(e))