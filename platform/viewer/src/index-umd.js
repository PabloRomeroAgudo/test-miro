/**
 * Entry point index.js for UMD packaging
 */
import 'regenerator-runtime/runtime';

import ReactDOM from 'react-dom';
import HomePage from './consensus-interface/components/HomePage.js';
import DataSet from './consensus-interface/data/DataSet.js';
import {SETTING_NAMES, MainSettings} from './consensus-interface/utils/MainSettings.js';

import './consensus-interface/theme-tide.css';

function installViewer(config, containerId = 'root', callback) {
  const container = document.getElementById(containerId);

  if (!container) {
    throw new Error(
      "No root element found to install viewer. Please add a <div> with the id 'root', or pass a DOM element into the installViewer function."
    );
  }

  return ReactDOM.render(<HomePage />, container, callback);
}

const Viewer = { DataSet, HomePage, installViewer, SETTING_NAMES, MainSettings };
export {Viewer};
export default Viewer;

