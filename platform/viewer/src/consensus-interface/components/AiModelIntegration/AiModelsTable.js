import React, { useMemo } from 'react';
import { Button, Select } from 'antd';
import { FiDelete } from 'react-icons/fi';
import { useTable, useRowSelect, useFilters, useSortBy, usePagination } from 'react-table';
import { ColumnFilter } from '../../utils/Filters.js';
import { MdOutlineMode } from 'react-icons/md';
import { CgAddR } from 'react-icons/cg';
import { confirmAlert } from 'react-confirm-alert';
import './styles.css';

export const AiModelsTable = ({ aiModels, onClickModifyModel, onClickRunPrediction, onClickDeleteAiModel, onClickAddNewModel, canPredict }) => {
  const headerStyle = {
    border: 'solid 1px black',
    textAlign: 'center',
    fontSize: '14px',
    padding: '2px',
    background: 'grey',
    color: 'white',
  };

  const tdStyle = {
    padding: '1px',
    border: 'solid 1px grey',
    fontSize: '14px',
    textAlign: 'center',
    cursor: 'default',
    color: 'white',
  };

  const columns = useMemo(
    () => [
      {
        Header: 'Name',
        accessor: 'name',
      },
      {
        Header: 'Author',
        accessor: 'author',
      },
      {
        Header: 'Organisation',
        accessor: 'organisation',
      },
      {
        Header: 'Category',
        accessor: 'category',
      },
      {
        Header: 'Remark',
        accessor: 'remark',
      },
      {
        Header: '',
        accessor: 'builtin',
      },
    ],
    []
  );

  const data = aiModels;
  const defaultColumn = React.useMemo(
    () => ({
      Filter: ColumnFilter,
    }),
    []
  );

  const deleteSelectedAiModel = (row) => {
    onClickDeleteAiModel(row.id);
  };

  const runPredictionAiModel = (row) => {
    if (canPredict) {
      onClickRunPrediction(row.id);
    } else {
      return confirmAlert({
        title: 'Run Prediction',
        message: 'You must first load a study before you can run a prediction model.',
        closeOnEscape: false,
        closeOnClickOutside: false,
        buttons: [
          {
            label: 'OK',
            onClick: () => {},
          },
        ],
      });
    }
  };

  const onPDFUploadHandler = (event) => {
    const file = event.target.files >= 0 ? event.target.files[0] : null;
    if (file) {
      const filename = file.name.trim();
      if (filename.toUpperCase().endsWith('pdf')) {
        alert('show/save pdf file: modal ongoing');
      }
    }
  };

  const callModifyModelInformation = (rowId) => {
    onClickModifyModel(rowId);
  };

  const callAddNewModel = (event) => {
    onClickAddNewModel(event);
  };

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    canNextPage,
    canPreviousPage,
    pageOptions,
    gotoPage,
    pageCount,
    setPageSize,
    prepareRow,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      stateReducer: (newState, action) => {
        if (action.type === 'toggleRowSelected') {
          newState.selectedRowIds = {
            [action.name]: true,
          };
        }
        return newState;
      },
      defaultColumn,
      autoResetSelectedRows: false,
      initialState: {
        sortBy: [
          {
            id: 'name',
            desc: false,
          },
        ],
        pageIndex: 0,
      },
    },
    useFilters,
    useSortBy,
    usePagination,
    useRowSelect,
    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        {
          id: 'description',
          Header: 'Description',
          Cell: ({ row }) => (
            <div className="uploadPdfButtonsDiv">
              <input
                id="uploadPdfInputId"
                type="file"
                name="uploadedPdf"
                multiple
                hidden
                onChange={(event) => onPDFUploadHandler(row)}
                style={{ width: '10px' }}
              />
              <label
                htmlFor="uploadPdfInputId"
                className="pdfButtonUpload"
                style={{
                  cursor: 'pointer',
                  fontSize: '15px',
                  fontWeight: 'bold',
                  color: 'white',
                  backgroundColor: 'black',
                }}
              >
                PDF
              </label>
            </div>
          ),
        },
        //...columns,
        columns[0],
        columns[1],
        columns[2],
        columns[3],
        columns[4],
        {
          id: 'modify',
          Cell: ({ row }) => (
            <MdOutlineMode
              className="addNewModelModifyTD"
              style={{
                fontSize: '30px',
                color: 'white',
                width: '50px',
                cursor: 'pointer',
              }}
              value={row}
              title="Modify the AI model Information"
              onClick={() => callModifyModelInformation(row.id)}
            ></MdOutlineMode>
          ),
        },
        {
          id: 'runprediction',
          Cell: ({ row }) => (
            <Button
              className="aiModelRunPredictionDiv"
              onClick={(event) => runPredictionAiModel(row)}
              style={{
                color: canPredict ? 'white' : 'red',
                fontSize: '15px',
                fontWeight: 'bold',
                cursor: 'pointer',
                width: '100%',
                padding: '5px',
                height: '100%',
                color: 'yellow',
              }}
            >
              Prediction
            </Button>
          ),
        },
        columns[5],
      ]);
    }
  );
  return (
    <>
      <table className="aimodelTable" {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr className="aimodeltableHeaderTR" {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())} style={headerStyle}>
                  <div className="aimodelTableThDiv">
                    {column.render('Header')}
                    <span>{column.isSorted ? (column.isSortedDesc ? ' 🔽' : ' 🔼') : ''}</span>
                  </div>
                </th>
              ))}
            </tr>
          ))}
          {headerGroups.map((headerGroup) => (
            <tr className="aimodeltableHeaderTR" {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th style={headerStyle} className={'aiModelHeader' + column.id}>
                  {column.id === 'builtin' || column.id === 'runprediction' || column.id == 'description' || column.id == 'modify' ? null : (
                    <td>{column.render('Filter')}</td>
                  )}
                  {column.id === 'builtin' ? (
                    <td>{<CgAddR className="addNewModelRow" title="Add a new configuration" onClick={(event) => callAddNewModel(event)} />}</td>
                  ) : null}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody className="aimodelTableTBody" {...getTableBodyProps()}>
          {page.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()} className={'aimodelTableTR'}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()} style={tdStyle}>
                      {cell.column.id == 'builtin' && !cell.value ? (
                        <FiDelete
                          className={'deleteAiModelRowIcon'}
                          style={{
                            fontSize: '20px',
                            color: 'red',
                            width: '20px',
                            cursor: 'pointer',
                          }}
                          onClick={(event) => deleteSelectedAiModel(row)}
                        />
                      ) : (
                        cell.render('Cell')
                      )}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="paginationDiv">
        <span style={{ color: 'white', height: '30px', paddingTop: '2px' }}>
          {'Page '}
          <strong style={{ color: 'white', marginRight: '8px' }}>
            {pageIndex + 1} of {pageOptions.length}
          </strong>{' '}
        </span>
        <span style={{ height: '30px', color: 'white' }}>
          | Go to page{' '}
          <input
            type={'number'}
            defaultValue={pageIndex}
            value={pageIndex}
            onChange={(e) => {
              const pageIndex = e.target.value ? Number(e.target.value) : 0;
              gotoPage(pageIndex);
            }}
            style={{ width: '60px', color: 'black' }}
          />
        </span>
        <Select value={pageSize} onChange={(e) => setPageSize(Number(e.target.value))} style={{ color: 'black', marginLeft: '20px', width: '100px' }}>
          {[5, 10, 15, 20].map((pageSize) => (
            <option key={pageSize} value={pageSize} style={{ color: 'black', backgroundColor: 'white' }}>
              {'Show ' + pageSize}
            </option>
          ))}
        </Select>
        <Button onClick={() => gotoPage(0)} disabled={!canPreviousPage} style={{ color: 'white', marginLeft: '10px', marginRight: '5px' }}>
          {'<<'}
        </Button>
        <Button onClick={() => previousPage()} disabled={!canPreviousPage} style={{ color: 'white', marginRight: '5px' }}>
          Previous
        </Button>
        <Button onClick={() => nextPage()} disabled={!canNextPage} style={{ color: 'white', marginRight: '5px' }}>
          Next
        </Button>
        <Button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage} style={{ color: 'white' }}>
          {'>>'}
        </Button>
      </div>
    </>
  );
};
