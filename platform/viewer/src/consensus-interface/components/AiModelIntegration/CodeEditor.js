import React from 'react';
import PropTypes from 'prop-types';
import AceEditor from 'react-ace';
import './styles.css';

class CodeEditor extends React.PureComponent {
  constructor(props) {
    super(props);

    const { value } = this.props;

    this.state = {
      value,
    };
  }

  componentDidUpdate(prevProps) {
    const { value } = this.props;

    if (prevProps.value !== value) {
      this.onChange(value);
    }
  }

  onChange = (value) => {
    const { onChange } = this.props;

    this.setState({ value });
    onChange(value);
  };

  getValue = () => {
    const { value } = this.state;
    return value;
  };

  render() {
    const { value } = this.state;

    return (
      <AceEditor
        className="aceEditor"
        theme="monokai"
        mode="python"
        setOptions={{
          tabSize: 2,
          showLineNumbers: true,
        }}
        showGutter={true}
        highlightActiveLine={true}
        showPrintMargin={false}
        editorProps={{ $blockScrolling: true }}
        {...this.props}
        onChange={this.onChange}
        value={value}
        style={{
          width: '99%',
          height: '750px',
          fontSize: '10px',
        }}
      />
    );
  }
}

CodeEditor.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
};

CodeEditor.defaultProps = {
  value: '',
  onChange: () => null,
};

export default CodeEditor;
