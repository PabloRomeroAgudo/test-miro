import React, { useState, useEffect } from 'react';
import { Button, Modal } from 'antd';
import { FileHelper, setChonkyDefaults } from 'chonky';
import { FileBrowser, ChonkyActions, FileNavbar, FileToolbar, FileList } from 'chonky';
import { ChonkyIconFA } from 'chonky-icon-fontawesome';
import './styles.css';

setChonkyDefaults({ iconComponent: ChonkyIconFA });

async function getDirectoryContents(currentPath) {
  const response = await fetch('/aimodels/dir', {
    method: 'POST',
    body: JSON.stringify({ path: currentPath }),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  if (response.status >= 400) {
    throw await response.json();
  }

  const data = await response.json();

  var contents = [];
  if (data.dirnames) {
    data.dirnames.forEach((f) =>
      contents.push({
        id: data.absolutepath + data.pathsep + f,
        name: f,
        isDir: true,
      })
    );
  }
  if (data.filenames) {
    data.filenames.forEach((f) =>
      contents.push({
        id: data.absolutepath + data.pathsep + f,
        name: f,
        isDir: false,
      })
    );
  }

  var folderChain = [];
  var folderPath = '';
  for (var i = 0; i < data.pathparts.length; i++) {
    if (i == 0) {
      folderPath = data.pathparts[0];
    } else {
      folderPath = folderPath + data.pathsep + data.pathparts[i];
    }
    folderChain.push({
      id: folderPath,
      name: data.pathparts[i],
      isDir: true,
    });
  }

  return {
    absolutePath: data.absolutepath,
    folderChain: folderChain,
    contents: contents,
    pathSeparator: data.pathsep,
  };
}

export const AIFileBrowser = (props) => {
  const [files, setFiles] = useState([]);
  const [folderChain, setFolderChain] = useState([]);
  const [selectedFile, setSelectedFile] = useState(null);
  const [error, setError] = useState(null);

  var pathSeparator = '/';
  var currentPath = props.path;

  const changeDirectory = function (newPath) {
    getDirectoryContents(newPath)
      .then((directoryContents) => {
        currentPath = directoryContents.absolutePath;
        pathSeparator = directoryContents.pathSeparator;
        setFiles(directoryContents.contents);
        setFolderChain(directoryContents.folderChain);
        if (props.selectDir) {
          props.updateSelectFile(currentPath);
        }
      })
      .catch((error) => setError(error.message));
  };

  useEffect(() => changeDirectory(currentPath), [props.path]);

  const handleFileAction = React.useCallback(
    (action) => {
      if (action.id === ChonkyActions.MouseClickFile.id) {
        if (action.payload.file.isDir) {
          changeDirectory(action.payload.file.id);
        } else {
          if (!props.selectDir) {
            setSelectedFile(action.payload.file.id);
            props.updateSelectFile(action.payload.file.id);
          }
        }
      } else if (action.id === ChonkyActions.OpenParentFolder.id) {
        changeDirectory(currentPath + pathSeparator + '..');
      } else if (action.id === ChonkyActions.OpenFiles.id) {
        changeDirectory(action.payload.files[0].id);
      }
    },
    [currentPath, pathSeparator, setSelectedFile, props.updateSelectFile]
  );

  return (
    <div style={{ height: 500 }}>
      {error && (
        <div>
          An error has occurred while reading the directory content: <strong style={{ color: 'red' }}>{error}</strong>
        </div>
      )}
      <FileBrowser
        files={files}
        disableDefaultFileActions={[ChonkyActions.OpenSelection.id, ChonkyActions.SelectAllFiles.id, ChonkyActions.ClearSelection.id]}
        disableDragAndDrop={true}
        darkMode={true}
        disableSelection={true}
        onFileAction={handleFileAction}
        folderChain={folderChain}
      >
        <FileNavbar />
        <FileToolbar />
        <FileList />
      </FileBrowser>
      <div style={{ color: 'orange', marginTop: '7px' }}>{props.selectDir ? null : <div>Selected file: {selectedFile}</div>}</div>
    </div>
  );
};

export default class AiFileBrowserDialog extends React.Component {
  constructor(props) {
    super(props);
    props.aiFileBrowserDialogFunction.setVisible = (isVisible, pathName, currentPath, selectDir) =>
      this.setVisible(isVisible, pathName, currentPath, selectDir);

    this.state = {
      visible: false,
      pathName: '',
      currentPath: '',
      selectedFile: '',
      selectDir: false,
    };
    this._isMounted = false;
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate(prevProps) {
    this.props.aiFileBrowserDialogFunction.setVisible = (isVisible, pathName, currentPath, selectDir) =>
      this.setVisible(isVisible, pathName, currentPath, selectDir);
  }

  setVisible(isVisible, pathName, currentPath, selectDir) {
    this.setState({
      visible: isVisible,
      pathName: pathName,
      currentPath: currentPath,
      selectDir: selectDir,
    });
  }

  handleCancel() {
    this.setState({ visible: false });
  }

  handleOK() {
    this.setState({ visible: false });
    this.props.setSelectedFile(this.state.selectedFile, this.state.pathName);
  }

  updateSelectFile(file) {
    this.setState({ selectedFile: file });
  }

  render() {
    return (
      <div>
        <Modal
          className="aiFiBrowserDialog"
          open={this.state.visible}
          destroyOnClose={true}
          closable={false}
          onOk={() => this.handleOK()}
          onCancel={() => this.handleCancel()}
          footer={[
            <Button key="cancel" onClick={() => this.handleCancel()}>
              Cancel
            </Button>,
            <Button key="ok" onClick={() => this.handleOK()} disabled={this.state.selectDir ? false : this.state.selectedFile.trim().length == 0}>
              Ok
            </Button>,
          ]}
        >
          <AIFileBrowser
            updateSelectFile={(file) => this.updateSelectFile(file)}
            path={this.state.currentPath}
            selectDir={this.state.selectDir}
          ></AIFileBrowser>
        </Modal>
      </div>
    );
  }
}
