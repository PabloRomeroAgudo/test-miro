import React from 'react';
import { Button, Modal } from 'antd';
import AiFileBrowserDialog from './AiFileBrowserDialog';
import CodeEditorDialog from './CodeEditorDialog';
import { Input } from 'antd';
import { FcStart } from 'react-icons/fc';
import { confirmAlert } from 'react-confirm-alert';
import ContourMappingDialog from './ContourMappingDialog';
import PropTypes from 'prop-types';
import uniqBy from 'lodash/uniqBy';

import './styles.css';

async function getFileContent(currentFilePath) {
  const response = await fetch('/aimodels/file', {
    method: 'POST',
    body: JSON.stringify({ path: currentFilePath }),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  if (response.status == 200) {
    const data = await response.json();
    return data.content;
  } else {
    throw new Error(response.statusText);
  }
}

export class SetCodePathTable extends React.Component {
  constructor(props) {
    super(props);

    SetCodePathTable.propTypes = {
      model: PropTypes.object,
      onModelModified: PropTypes.func,
      setSelectedFile: PropTypes.func,
      activeStudyInstanceUID: PropTypes.string,
      study: PropTypes.array,
    };

    this.state = {
      runModel: { ...this.props.model },
      code: '',
    };

    this.aiFileBrowserDialogFunction = {};
    this.codeEditorDialogFunctions = {};
    this.handleChangePrediction = this.handleChangePrediction.bind(this);

    this.predictDirRef = React.createRef();
    this.tempDirRef = React.createRef();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.model !== this.props.model) {
      this.setState({
        runModel: { ...this.props.model },
      });
    }

    this.predictDirRef.current.input.selectionStart = this.predictDirCursorPos;
    this.predictDirRef.current.input.selectionEnd = this.predictDirCursorPos;
    this.tempDirRef.current.input.selectionStart = this.tempDirCursorPos;
    this.tempDirRef.current.input.selectionEnd = this.tempDirCursorPos;
  }

  renderTableHeader(headerName) {
    return (
      <th
        className={'setCodePath' + headerName + 'TH'}
        rowSpan="1"
        style={{
          border: 'solid 1px black',
          textAlign: 'center',
          fontSize: '12px',
          padding: '2px',
          background: 'grey',
          color: 'white',
        }}
      >
        {headerName}
      </th>
    );
  }

  onClickAiFileBrowserDialog(pathName, currentPath, selectDir) {
    this.aiFileBrowserDialogFunction.setVisible(true, pathName, currentPath, selectDir);
  }

  setChangePath(path, pathName) {
    var newModelPaths = { ...this.state.runModel.modelPaths };
    newModelPaths[pathName] = path;
    var newModel = { ...this.state.runModel, modelPaths: newModelPaths };

    this.props.onModelModified(newModel);
  }

  handleOpenCodeEditor(path) {
    // check whether user has provided a complete mapping
    const model = this.state.runModel;
    if (model.category.toLowerCase() === 'doseprediction') {
      const isMappingComplete =
        model.oarChannels.every((channel) => model.oarChannelMapping[channel]) && model.tarChannels.every((channel) => model.tarChannelMapping[channel]);
      if (!isMappingComplete) {
        confirmAlert({
          className: 'confirmAlsertOpenCode',
          title: 'Prediction',
          message:
            'The mapping of available contours to model inputs is not complete! You have to provide a complete mapping before you can run the prediction.',
          closeOnEscape: false,
          closeOnClickOutside: false,
          buttons: [
            {
              label: 'OK',
              onClick: () => {},
            },
          ],
        });
        return;
      }
    }

    getFileContent(path)
      .then((sourcecode) => {
        this.setState({ code: sourcecode });
        this.codeEditorDialogFunctions.setVisible(true);
      })
      .catch((error) => {
        confirmAlert({
          className: 'confirmAlsertOpenCode',
          title: 'Prediction',
          message: error.message,
          closeOnEscape: false,
          closeOnClickOutside: false,
          buttons: [
            {
              label: 'OK',
              onClick: () => {},
            },
          ],
        });
      });
  }

  handleChangePrediction(e) {
    this.setChangePath(e.target.value, 'predictionPath');
  }

  handleChangeTemporaryDirectory(e) {
    this.setChangePath(e.target.value, 'temporaryDirectoryPath');
  }

  getImageSetSeriesInstanceUIDByModality(st, modality) {
    if (!st) return null;

    const allSeries = uniqBy(st, 'SeriesInstanceUID');
    if (!allSeries.length) return null;

    const CTUIds = allSeries.filter((study) => study.modalities == modality).map((study) => study.SeriesInstanceUID);

    let CTUId;
    if (CTUIds.length) CTUId = CTUIds[0];
    else CTUId = allSeries.filter((study) => study.modalities == modality).map((study) => study.SeriesInstanceUID)[0];

    return CTUId;
  }

  // Get the rtstruct serial instance uid, can only run the first
  // TO DO return all the serial instance IDd
  getSeriesInstanceUIDByModality(st, modality) {
    if (!st) return null;

    const allSeries = uniqBy(st, 'SeriesInstanceUID');
    if (!allSeries.length) return null;

    const RTSTRUCTUIds = allSeries.filter((study) => study.modalities == modality).map((study) => study.SeriesInstanceUID);

    return RTSTRUCTUIds[0];
  }

  renderTableTBody() {
    const tdStyle = {
      padding: '1px',
      border: 'solid 1px grey',
      fontSize: '14px',
      textAlign: 'left',
      width: '220px',
    };

    const selectionBtnStyle = {
      padding: '1px',
      fontSize: '14px',
      textAlign: 'center',
      width: '52px',
      backgroundColor: '#177DDC',
      height: '52px',
    };

    return (
      <tbody>
        <tr className="predictPathTR">
          <td style={{ textAlign: 'center' }}>Prediction</td>
          <td>
            <div className="predictionTdDiv" style={{ display: 'inline-flex' }}>
              <Input
                ref={this.predictDirRef}
                type="text"
                value={this.state.runModel.modelPaths.predictionPath}
                style={{ width: '569px' }}
                placeholder="Enter the path to the predicted code"
                onChange={(e) => {
                  this.predictDirCursorPos = e.target.selectionStart;
                  this.handleChangePrediction(e);
                }}
              />
            </div>
          </td>
          {this.state.runModel.modelPaths.predictionPath.trim() !== '' ? (
            <td>
              <FcStart
                className="runningBtn"
                onClick={() => {
                  this.handleOpenCodeEditor(this.state.runModel.modelPaths.predictionPath);
                }}
                title="START prediction process"
                size={50}
              />
            </td>
          ) : (
            <div style={{ width: '50px' }} />
          )}
          <td className="selectPredictionTD" style={tdStyle}>
            <Button
              className="selectPredcitonPathBtn"
              style={selectionBtnStyle}
              onClick={() => this.onClickAiFileBrowserDialog('predictionPath', this.state.runModel.modelPaths.predictionPath, false)}
              title={'Select the prediction script path'}
            >
              ...
            </Button>
          </td>
        </tr>
        <tr className="temporaryPathTR">
          <td style={{ textAlign: 'center' }}>Temporary</td>
          <td>
            <div className="temporaryTdDiv" style={{ display: 'inline-flex' }}>
              <Input
                ref={this.tempDirRef}
                type="text"
                value={this.state.runModel.modelPaths.temporaryDirectoryPath}
                style={{ width: '569px' }}
                placeholder="Entry the necessary temporary path"
                onChange={(e) => {
                  this.tempDirCursorPos = e.target.selectionStart;
                  this.handleChangeTemporaryDirectory(e);
                }}
              />
            </div>
          </td>
          <div />
          <td className="selectTemporaryDirectoryTD" style={tdStyle}>
            <Button
              className="selectTemporaryDirectoryPathBtn"
              style={selectionBtnStyle}
              onClick={() => this.onClickAiFileBrowserDialog('temporaryDirectoryPath', this.state.runModel.modelPaths.temporaryDirectoryPath, true)}
              title={'Select the temporary directory path'}
            >
              ...
            </Button>
          </td>
        </tr>
      </tbody>
    );
  }

  render() {
    return (
      <div>
        <table className="setCodePathTable">
          <thead>
            <tr className="setCodePathTableTR">
              {this.renderTableHeader('Script')}
              {this.renderTableHeader('Path')}
              {this.renderTableHeader('')}
            </tr>
          </thead>
          {this.renderTableTBody()}
        </table>
        <AiFileBrowserDialog
          className="aiFiBrowserDialog"
          setSelectedFile={(file, pathName) => this.setChangePath(file, pathName)}
          aiFileBrowserDialogFunction={this.aiFileBrowserDialogFunction}
        />
        <CodeEditorDialog
          codeEditorDialogFunctions={this.codeEditorDialogFunctions}
          code={this.state.code}
          predictionPath={this.state.runModel.modelPaths.predictionPath}
          temporaryDirectoryPath={this.state.runModel.modelPaths.temporaryDirectoryPath}
          category={this.state.runModel.category}
          oarChannelMapping={this.state.runModel.oarChannelMapping}
          tarChannelMapping={this.state.runModel.tarChannelMapping}
          patientName={this.props.study ? this.props.study[0].PatientName : ''}
          ctSeriesInstanceUID={this.getImageSetSeriesInstanceUIDByModality(this.props.study, 'CT')}
          rtStructSeriesInstanceUID={this.getSeriesInstanceUIDByModality(this.props.study, 'RTSTRUCT')}
          regSeriesInstanceUID={this.getSeriesInstanceUIDByModality(this.props.study, 'REG')}
          activeStudyInstanceUID={this.props.activeStudyInstanceUID}
        />
      </div>
    );
  }
}

export default class RunPredictionDialog extends React.Component {
  constructor(props) {
    super(props);

    RunPredictionDialog.propTypes = {
      runPredictionDialogFunctions: PropTypes.object,
      activeStudyInstanceUID: PropTypes.string,
      model: PropTypes.object,
      onModelModified: PropTypes.func,
      study: PropTypes.array,
    };

    props.runPredictionDialogFunctions.setVisible = (isVisible) => this.setVisible(isVisible);

    this.state = {
      visible: false,
      codeEditorVisible: false,
      runModel: { ...this.props.model },
      showError: false,
    };
    this.contourMappingDialogFunctions = {};
  }

  componentDidUpdate(prevProps) {
    this.props.runPredictionDialogFunctions.setVisible = (isVisible) => this.setVisible(isVisible);
    if (prevProps.model !== this.props.model) {
      this.setState({
        runModel: { ...this.props.model },
      });
    }
  }

  setVisible(isVisible) {
    this.setState({
      visible: isVisible,
      showError: false,
    });
  }

  onCheckboxChange(selectedContours) {
    if (selectedContours) {
      this.setState({
        runModel: {
          ...this.state.runModel,
          contours: selectedContours,
        },
      });
    }
  }

  handleOK() {
    if (this.state.runModel.modelPaths.predictionPath.trim() == '' || this.state.runModel.modelPaths.temporaryDirectoryPath.trim() == '') {
      this.setState({ showError: true });
    } else {
      this.props.onModelModified(this.state.runModel);
      this.setVisible(false);
    }
  }

  handleCancel() {
    this.setState({ runModel: { ...this.props.model } });
    this.setVisible(false);
  }

  handleModelModified(newModel) {
    this.setState({ runModel: { ...newModel } });
  }

  showErrorMessage() {
    if (this.state.showErrorPredition) {
      return <h4 className="errorMessage">Please set the necessary prediction and temporary directory paths!</h4>;
    } else {
      return <div />;
    }
  }

  getShowContourMappingDialogButton() {
    if (this.state.runModel.category === 'dosePrediction') {
      return (
        <Button
          className="mappingContoursBtn"
          style={{ color: 'yellow', width: '350px', fontSize: '16px', height: '40px' }}
          onClick={() => {
            this.contourMappingDialogFunctions.setVisible(true);
          }}
        >
          Match available contours to AI model inputs
        </Button>
      );
    }
  }

  render() {
    return (
      <div>
        <Modal
          className="runPredictionDialog"
          open={this.state.visible}
          destroyOnClose={true}
          closable={false}
          onOk={() => this.handleOK()}
          footer={[
            <Button key="cancel" onClick={() => this.handleCancel()}>
              Cancel
            </Button>,
            <Button key="ok" onClick={() => this.handleOK()}>
              OK
            </Button>,
          ]}
        >
          <h1 className="predictionDialogTitle">AI model prediction: Configuration</h1>
          {this.showErrorMessage()}
          <h4 className="predictionNotificationLabel" style={{ color: '#C16628' }}>
            Set the path storing your scripts. The temporary directory path is for the results except for the predicted DICOM which is stored on the internal
            server. Contour mapping check if the code editor cannot be opened!
          </h4>
          <SetCodePathTable
            setSelectedFile={(file, pathName) => this.setSelectedFile(file, pathName)}
            model={this.state.runModel}
            onModelModified={(modifiedModel) => {
              this.handleModelModified(modifiedModel);
            }}
            activeStudyInstanceUID={this.props.activeStudyInstanceUID}
            study={this.props.study}
          />
          {this.getShowContourMappingDialogButton()}
          <ContourMappingDialog
            contourMappingDialogFunctions={this.contourMappingDialogFunctions}
            model={this.state.runModel}
            onModelModified={(modifiedModel) => {
              this.handleModelModified(modifiedModel);
            }}
          />
        </Modal>
      </div>
    );
  }
}
