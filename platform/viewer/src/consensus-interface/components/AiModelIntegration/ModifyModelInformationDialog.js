import React, { useRef } from 'react';
import { Button, Modal, Input, Select } from 'antd';
import { BsArrowDownCircle } from 'react-icons/bs';
import ChannelDefinitionDialog from './ChannelDefinitionDialog';
var XLSX = require('xlsx');
import './styles.css';

/* list of supported file types */
// const SheetJSFT = ['xlsx', 'xlsb', 'xlsm', 'xls', 'csv', 'txt']
//   .map(function (x) {
//     return '.' + x;
//   })
//   .join(',');

export class ModifyModelInformationTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modelName: this.props.model.name,
      author: this.props.model.author,
      organisation: this.props.model.organisation,
      category: this.props.model.category,
      remark: this.props.model.remark,
      pdfPath: this.props.model.pdfPath,
    };
  }

  // handleContourListFileSelected(e) {
  //   const files = e.target.files;
  //   if (files && files[0]) {
  //     const file = files[0];
  //     const reader = new FileReader();
  //     const rABS = !!reader.readAsBinaryString;
  //     reader.onload = (e) => {
  //       /* Parse data */
  //       const bstr = e.target.result;
  //       const wb = XLSX.read(bstr, { type: rABS ? 'binary' : 'array' });

  //       /* Get first worksheet */
  //       const wsname = wb.SheetNames[0];
  //       const ws = wb.Sheets[wsname];
  //       // console.log(rABS, wb);

  //       /* Convert array of arrays */
  //       const data = XLSX.utils.sheet_to_json(ws, { header: 1 });
  //       this.props.onNewContourListRead(data);
  //     };
  //     if (rABS) {
  //       reader.readAsBinaryString(file);
  //     } else {
  //       reader.readAsArrayBuffer(file);
  //     }
  //   }
  // }

  renderTableHeader(headerName) {
    return (
      <th
        className={'defineNewModel' + headerName + 'TH'}
        rowSpan="1"
        style={{
          border: 'solid 1px black',
          textAlign: 'center',
          fontSize: '12px',
          padding: '2px',
          background: 'grey',
          color: 'white',
        }}
      >
        {headerName}
      </th>
    );
  }

  changeModelName(event) {
    this.setState({
      modelName: event.target.value,
    });
    this.props.model.name = event.target.value.trim();
  }

  changeAuthor(event) {
    this.setState({
      author: event.target.value,
    });
    this.props.model.author = event.target.value.trim();
  }

  changeOrganisation(event) {
    this.setState({
      organisation: event.target.value,
    });
    this.props.model.organisation = event.target.value.trim();
  }

  changeRemark(event) {
    this.setState({
      remark: event.target.value,
    });
    this.props.model.remark = event.target.value.trim();
  }

  changePdfPath(event, pdfPath) {
    const file = e.target.files[0].name;
    if (file) {
      this.setState({
        pdfPath: pdfPath,
      });
    }
    this.props.model.pdfPath = pdfPath;
  }

  handleChangeCategory(value) {
    this.setState({ category: value });
    this.props.model.category = value;
    this.props.onChangeCategory();
  }

  // handleSelectedFileChange(e) {
  //   const files = e.target.files;
  //   if (files && files[0]) {
  //     this.handleContourListFile(files[0]);
  //     // this.setState({ enableMappingBtn: true });
  //   } else {
  //     // this.setState({ enableMappingBtn: false });
  //   }
  // }

  // getContourMappingElement(tdStyle) {
  //   return this.state.category === 'dosePrediction' ? (
  //     <tr className="dosePredictionContoursTR">
  //       <td className="dosePredictionContourTD" style={tdStyle} title="Mapping of prediction contours and application contours">
  //         Contour mapping
  //       </td>
  //       <td className="viewMappingContourTD" style={tdStyle} title="View the mapping contours">
  //         <div style={{ display: 'inline-flex' }}>
  //           <Input
  //             className="uploadContourExcelInput"
  //             id="uploadContourExcelInputId"
  //             type="file"
  //             title="Select Excel file with contour names"
  //             name="uploadContourExcel"
  //             accept={SheetJSFT}
  //             onChange={(event) => this.handleContourListFileSelected(event)}
  //             style={{
  //               color: 'white',
  //               width: '420px',
  //             }}
  //           />
  //           <Button
  //             className="doMappingBtn"
  //             style={{ color: 'yellow', width: '40%' }}
  //             onClick={() => {
  //               this.props.contourDefinitionDialogFunctions.setVisible(true);
  //             }}
  //           >
  //             Edit mapping
  //           </Button>
  //         </div>
  //       </td>
  //     </tr>
  //   ) : null;
  // }

  renderTableTBody() {
    const tdStyle = {
      padding: '1px',
      fontSize: '14px',
      textAlign: 'center',
      width: '30px',
    };

    let inputPdfRef = useRef < HTMLInputElement > null;
    const { Option } = Select;

    return (
      <tbody className="addNewModelTableTBody">
        <tr className="aiModelNameTR">
          <td className="aiModelNameTD" style={tdStyle} title="Enter the name of the AI model">
            Model Name
          </td>
          <td className="enterModelNameTD" style={tdStyle}>
            <Input
              className="enterModelName"
              type="text"
              defaultValue={this.state.modelName}
              onChange={(event) => this.changeModelName(event)}
              style={{ color: 'white', width: '100%' }}
            ></Input>
          </td>
        </tr>
        <tr className="aiModelAuthoTR">
          <td className="aiModelAuthorTD" style={tdStyle} title="Enter the author of the AI model">
            Author
          </td>
          <td className="enterModelAuthorTD" style={tdStyle}>
            <Input
              className="enterModelAuthor"
              type="text"
              defaultValue={this.state.author}
              onChange={(event) => this.changeAuthor(event)}
              style={{ color: 'white', width: '100%' }}
            ></Input>
          </td>
        </tr>
        <tr className="aiModelOrganisationTR">
          <td className="aiModelOrganisationTD" style={tdStyle} title="Enter the organisation for the AI model">
            Organisation
          </td>
          <td className="enterOrganisationTD" style={tdStyle}>
            <Input
              className="enterOrganisation"
              type="text"
              defaultValue={this.state.organisation}
              onChange={(event) => this.changeOrganisation(event)}
              style={{ color: 'white', width: '100%' }}
            ></Input>
          </td>
        </tr>
        <tr className="aiModelCategoryTR">
          <td className="aiModelCategoryTD" style={tdStyle} title="Enter the category of the AI model">
            Category
          </td>
          <td className="enterCategoryTD" style={tdStyle}>
            <Select
              showSearch
              closable
              suffixIcon={<BsArrowDownCircle style={{ color: 'yellow' }} size="30px" />}
              style={{ width: '100%', textAlign: 'left' }}
              dropdownStyle={{ backgroundColor: 'white' }}
              placeholder={<div style={{ color: 'white' }}>Select Prediction Category</div>}
              defaultValue={this.state.category}
              optionFilterProp="children"
              onChange={(value) => this.handleChangeCategory(value)}
            >
              <Option value="dosePrediction" style={{ backgroundColor: 'white', color: 'black' }}>
                Dose Prediction
              </Option>
              <Option value="contourPrediction" style={{ backgroundColor: 'white', color: 'black' }}>
                Contour Prediction
              </Option>
            </Select>
          </td>
        </tr>
        <tr className="aiModelRemarkTR">
          <td className="aiModelRemarkTD" style={tdStyle} title="Enter a remark to the AI model">
            Remark
          </td>
          <td className="enterRemarkTD" style={tdStyle}>
            <Input
              className="enterRemark"
              type="text"
              defaultValue={this.state.remark}
              onChange={(event) => this.changeRemark(event)}
              style={{ color: 'white', width: '100%' }}
            />
          </td>
        </tr>
        <tr className="aiModelPdfTR">
          <td className="aiModelPdfTD" style={tdStyle} title="Select a PDF file with the description of the AI model">
            PDF
          </td>
          <td className="selectPDFTD" style={tdStyle}>
            <Input
              className="enterPdfPath"
              id="uploadPdfInputId"
              type="file"
              name="uploadedPdf"
              accept=".pdf"
              onChange={(event) => this.changePdfPath(event, inputPdfRef)}
              style={{ color: 'white', width: '100%' }}
            />
          </td>
        </tr>
      </tbody>
    );
  }

  render() {
    return (
      <table
        className="modifyModelInfoTable"
        style={{
          border: 'sold 1px grey',
          fontSize: '14px',
          padding: '2px',
          textAlign: 'left',
          borderCollapse: 'inherit',
          width: '100%',
          paddingBottom: '10px',
        }}
      >
        <thead>
          <tr className="modifyModelInfoTableTR">
            {this.renderTableHeader('Information')}
            {this.renderTableHeader('Value')}
          </tr>
        </thead>
        {this.renderTableTBody()}
      </table>
    );
  }
}

export default class ModifyModelInformationDialog extends React.Component {
  constructor(props) {
    super(props);
    props.modifyModelDialogFunction.setVisible = (isVisible) => this.setVisible(isVisible);

    this.state = {
      visible: false,
      activeStudyInstanceUID: this.props.activeStudyInstanceUID,
      editedModel: { ...this.props.model }, // make a copy of the model
      checked: true,
      errorMessage: null,
      isShowSetInputChannel: false,
    };
    this.contourDefinitionDialogFunctions = {};
  }

  componentDidUpdate(prevProps) {
    this.props.modifyModelDialogFunction.setVisible = (isVisible) => this.setVisible(isVisible);

    // this is necessary because when the dialog is created first, the model is undefined
    if (prevProps.model != this.props.model) {
      this.setState({
        editedModel: { ...this.props.model },
      });
    }
  }

  // onNewContourListRead(newContourList) {
  //   const contourMapping = this.state.editedModel.contourMapping;

  //   // ignore first entry (it's a table header line)
  //   for (var i = 1; i < newContourList.length; i++) {
  //     const contourName = newContourList[i][0];
  //     // add new contours that are not already in the mapping
  //     if (!contourMapping.hasOwnProperty(contourName)) {
  //       contourMapping[contourName] = null;
  //     }
  //   }
  //   // remove contours from the mapping that are not in the list
  //   Object.getOwnPropertyNames(contourMapping).forEach((mappedContourName) => {
  //     if (!newContourList.find((entry) => entry[0] === mappedContourName)) {
  //       delete contourMapping[mappedContourName];
  //     }
  //   });

  //   const newEditedModel = { ...this.state.editedModel };
  //   newEditedModel.contourMapping = { ...contourMapping };
  //   this.setState({ editedModel: newEditedModel });
  // }

  setVisible(isVisible) {
    this.setState({
      visible: isVisible,
    });
  }

  handleOk() {
    if (this.state.editedModel.name == '') {
      this.setState({ checked: false, visible: true, errorMessage: 'The model name must not be empty' });
    } else if (this.state.editedModel.category == '') {
      this.setState({ checked: false, visible: true, errorMessage: 'The model category must not be empty' });
    } else {
      this.props.onModelModified(this.state.editedModel);
      this.setState({ checked: true, visible: false });
    }
  }

  handleCancel() {
    this.setState({
      editedModel: { ...this.props.model },
    });
    this.setState({ checked: true, visible: false });
  }

  render() {
    return (
      <div>
        <Modal
          className="addAiModelDialog"
          open={this.state.visible}
          destroyOnClose={true}
          closable={false}
          onOk={() => this.handleOk()}
          footer={[
            <Button key="cancel" onClick={() => this.handleCancel()}>
              Cancel
            </Button>,
            <Button key="ok" onClick={() => this.handleOk()}>
              Ok
            </Button>,
          ]}
        >
          <h1 className="modifyModelModalLabel" style={{ margin: '4px' }}>
            AI Model information
          </h1>
          {this.state.checked ? null : <h2 style={{ fontSize: '15px', color: 'red', margin: '4px' }}>{this.state.errorMessage}</h2>}
          <ModifyModelInformationTable
            className="modifyModelInformationTable"
            model={this.state.editedModel}
            // onNewContourListRead={(list) => this.onNewContourListRead(list)}
            onChangeCategory={() => this.setState({ editedMode: { ...this.state.editedModel } })} // force update of model
          />
          {this.state.editedModel.category === 'dosePrediction' ? (
            <Button
              className="doMappingBtn"
              style={{ color: 'yellow', width: '200px', fontSize: '16px', height: '40px' }}
              onClick={() => {
                this.contourDefinitionDialogFunctions.setVisible(true);
              }}
            >
              Set input channels
            </Button>
          ) : null}
          <ChannelDefinitionDialog
            className="contourDefinitionDialog"
            contourDefinitionDialogFunctions={this.contourDefinitionDialogFunctions}
            tarChannels={this.state.editedModel.tarChannels ? this.state.editedModel.tarChannels : []}
            oarChannels={this.state.editedModel.oarChannels ? this.state.editedModel.oarChannels : []}
            onDefineChannelChanged={(newTarChannels, newOarChannels) => {
              const newEditedModel = { ...this.state.editedModel };
              newEditedModel.tarChannels = [...newTarChannels];
              newEditedModel.oarChannels = [...newOarChannels];
              this.setState({ editedModel: newEditedModel });
            }}
          />
        </Modal>
      </div>
    );
  }
}
