import React from 'react';
import { Helmet, HelmetProvider } from 'react-helmet-async';
import { Button, Modal, Radio, Select } from 'antd';
import { CircularProgress } from '@mui/material';
import DataSet from './../../data/DataSet.js';
import CodeEditor from './CodeEditor';
import brace from 'brace';
import 'brace/mode/python';
import 'brace/theme/monokai';
import PropTypes from 'prop-types';
import './styles.css';

//  ************** api *************
async function runPrediction({
  predictionPath,
  interpreter,
  temporaryDirectoryPath,
  patientName,
  ctSeriesInstanceUID,
  rtStructSeriesInstanceUID,
  regSeriesInstanceUID,
  oarChannelMapping,
  tarChannelMapping,
  category,
}) {
  let oarList = [];
  let tvList = [];
  if (category.toLowerCase() == 'doseprediction') {
    oarList = Object.keys(oarChannelMapping).map((k) => oarChannelMapping[k].segment);
    tvList = Object.keys(tarChannelMapping).map((k) => tarChannelMapping[k].segment);
  }
  const response = await fetch('/aimodels/runprediction', {
    method: 'POST',
    body: JSON.stringify({
      path: predictionPath,
      interpreter: interpreter,
      temporaryDirectoryPath: temporaryDirectoryPath,
      patientName: patientName,
      ctSeriesInstanceUID: ctSeriesInstanceUID,
      rtStructSeriesInstanceUID: rtStructSeriesInstanceUID,
      regSeriesInstanceUID: regSeriesInstanceUID,
      oarList: oarList,
      tvList: tvList,
      category: category,
    }),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  if (response.status === 200) {
    const data = await response.json();
    return data;
  } else {
    throw new Error('Run prediction error');
  }
}

async function stopPrediction() {
  const response = await fetch('/aimodels/stopprediction', {
    method: 'POST',
    body: JSON.stringify({}),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  if (response.status === 200) {
    const data = await response.json();
    return data;
  } else {
    throw new Error('Stop prediction error');
  }
}

async function getPredictionOutput() {
  const response = await fetch('/aimodels/outprediction', {
    method: 'POST',
    body: JSON.stringify({}),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  if (response.status === 200) {
    const data = await response.json();
    return data;
  } else {
    throw new Error('Get log error');
  }
}

async function storePredictionCode(predictionCodeInfo) {
  const response = await fetch('/aimodels/savecode', {
    method: 'POST',
    body: JSON.stringify(predictionCodeInfo),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  if (response.status == 200) {
    const data = (await response.json()).message;
    return data;
  } else {
    const error = (await response.json()).message;
    throw error;
  }
}

// ********* Code editor dialog *******
const { Option } = Select;

const Scripts = (props) => {
  const { code } = props;
  return <script type="text/python">{code}</script>;
};

const output = (arr) => {
  let out = '';
  for (let i = 0; i < arr.length; i++) {
    if (i !== arr.length - 1) {
      out = out.concat(`${arr[i]}\n`);
    } else {
      out = out.concat(arr[i]);
    }
  }
  return out;
};

export default class CodeEditorDialog extends React.Component {
  constructor(props) {
    super(props);

    CodeEditorDialog.propTypes = {
      code: PropTypes.object,
      codeEditorDialogFunctions: PropTypes.object,
      predictionPath: PropTypes.string,
      temporaryDirectoryPath: PropTypes.string,
      patientName: PropTypes.string,
      activeStudyInstanceUID: PropTypes.string,
      ctSeriesInstanceUID: PropTypes.string,
      rtStructSeriesInstanceUID: PropTypes.string,
      regSeriesInstanceUID: PropTypes.string,
      category: PropTypes.string,
      oarChannelMapping: PropTypes.object,
      tarChannelMapping: PropTypes.object,
    };

    props.codeEditorDialogFunctions.setVisible = (isVisible) => this.setVisible(isVisible);

    this.logUpdateTimer = null;

    this.state = {
      visible: false,
      code: this.props.code,
      outputArr: [],
      interpreterType: 'parrotserver',
      interpreter: 'py3109',
      isUpdateStudy: false,
      isRunUpdateStudy: false,
    };

    this.onRadioValueChange = this.onRadioValueChange.bind(this);

    this.textLog = React.createRef();
  }

  componentDidUpdate(prevProps) {
    this.props.codeEditorDialogFunctions.setVisible = (isVisible) => this.setVisible(isVisible);
    if (prevProps.code != this.props.code) {
      this.setState({
        code: this.props.code,
      });
    }

    if (this.textLog.current) {
      this.textLog.current.scrollTop = this.textLog.current.scrollHeight;
    }
  }

  setVisible(isVisible) {
    this.setState({
      visible: isVisible,
    });
  }

  clearLogs() {
    this.setState({
      outputArr: [],
    });
  }

  setLogs(message) {
    this.setState({
      outputArr: [message],
    });
  }

  addToLogs(message) {
    if (message) {
      const newOutput = this.state.outputArr.slice();
      newOutput.push(message);
      this.setState({
        outputArr: newOutput,
      });
    }
  }

  addErrorToLogs(message) {
    this.addToLogs('*** Error: ' + message);
  }

  run() {
    this.stop(() => {
      const sourceCode = this.state.code;
      const predictionCodeInfo = { path: this.props.predictionPath, sourceCode: sourceCode };
      storePredictionCode(predictionCodeInfo)
        .then((result) => {
          this.addToLogs(result.message);
          return runPrediction({
            predictionPath: this.props.predictionPath,
            interpreter: this.state.interpreter,
            temporaryDirectoryPath: this.props.temporaryDirectoryPath,
            patientName: this.props.patientName,
            ctSeriesInstanceUID: this.props.ctSeriesInstanceUID,
            rtStructSeriesInstanceUID: this.props.rtStructSeriesInstanceUID,
            regSeriesInstanceUID: this.props.regSeriesInstanceUID,
            oarChannelMapping: this.props.oarChannelMapping,
            tarChannelMapping: this.props.tarChannelMapping,
            category: this.props.category,
          });
        })
        .then((result) => {
          this.setLogs('Prediction started: Running....');
          this.outputUpdateFunction = function () {
            getPredictionOutput()
              .then((outres) => {
                clearTimeout(this.logUpdateTimer);
                if (outres.isRunning) {
                  this.addToLogs(outres.output);
                  this.logUpdateTimer = setTimeout(this.outputUpdateFunction, 1000);
                } else {
                  this.setState({ isUpdateStudy: true });
                  this.addToLogs(outres.output + '\nPrediction process has terminated');
                }
              })
              .catch((outerr) => {
                this.addErrorToLogs(outerr.message);
                this.setState({ isUpdateStudy: false });
              });
          }.bind(this);
          this.setState({ isUpdateStudy: false });
          this.logUpdateTimer = setTimeout(this.outputUpdateFunction, 1000);
        })
        .catch((error) => {
          this.addErrorToLogs(error.message);
          this.setState({ isUpdateStudy: false });
        });
    });
  }

  async stop(onSuccess) {
    this.setState({ isUpdateStudy: false });
    return stopPrediction()
      .then((result) => {
        clearTimeout(this.logUpdateTimer);
        this.addToLogs('Process stopped');
        if (onSuccess) onSuccess();
      })
      .catch((error) => {
        clearTimeout(this.logUpdateTimer);
        this.addErrorToLogs(error.message);
      });
  }

  handleClose() {
    this.stop().finally(() => {
      const sourceCode = this.state.code;
      const predictionCodeInfo = { path: this.props.predictionPath, sourceCode: sourceCode };
      storePredictionCode(predictionCodeInfo)
        .then((result) => {
          this.addToLogs(result.message);
        })
        .catch((error) => this.addErrorToLogs(error.message))
        .finally(() =>
          this.setState({
            visible: false,
            isUpdateStudy: false,
          })
        );
    });
  }

  updateStudy() {
    this.stop().finally(() => {
      // Load the study again
      this.setState({
        isRunUpdateStudy: true,
      });
      DataSet.getInstance()
        .loadStudyAsync(this.props.activeStudyInstanceUID)
        .finally(() => {
          this.setState({
            isUpdateStudy: false,
            visible: false,
          });
        });
    });
  }

  onRadioValueChange(event) {
    this.setState({
      interpreterType: event.target.value,
      interpreter: event.target.value === 'local' ? 'local' : this.state.interpreter,
    });
  }

  handleSelectInterpreterChange(value) {
    this.setState({
      interpreter: value,
    });
  }

  renderSelectInterpreter() {
    const children = [<Option key="py368">Python3.6.8</Option>, <Option key="py3109">Python3.10.9</Option>];
    return (
      <div className="selectInterpreterDiv">
        <Select
          className="selectInterpreter"
          placeholder="Please select interpreter"
          defaultValue={this.state.interpreter}
          onChange={(value) => this.handleSelectInterpreterChange(value)}
          disabled={this.state.interpreterType === 'local'}
          dropdownStyle={{ backgroundColor: 'white' }}
        >
          {children}
        </Select>
      </div>
    );
  }

  renderInterpreter() {
    return (
      <div>
        <div className="selectAIServerDiv" style={{ display: 'inline-flex' }}>
          <Radio.Group value={this.state.interpreterType} onChange={this.onRadioValueChange}>
            <Radio value={'local'} className="radioLocal">
              Local
            </Radio>
            <Radio value={'parrotserver'} className="radioParrotServer">
              Interpreters:
            </Radio>
          </Radio.Group>
          <div className="selectInterpretersDiv">{this.renderSelectInterpreter()}</div>
        </div>
      </div>
    );
  }

  render() {
    const { code, outputArr } = this.state;
    return (
      <div>
        <Modal
          className="codeEditorDialog"
          open={this.state.visible}
          destroyOnClose={true}
          closable={false}
          onOk={() => this.handleOK()}
          footer={[
            this.state.isRunUpdateStudy ? (
              <CircularProgress
                className="circularProgress"
                size="2rem"
                style={{
                  color: 'red',
                  marginRight: '20px',
                }}
              />
            ) : null,
            <Button
              key="run"
              onClick={() => this.run()}
              disabled={this.state.isUpdateStudy}
              style={{ marginRight: '20px', color: !this.state.isUpdateStudy ? '#00FF00' : 'grey', fontWeight: 'bold' }}
            >
              RUN
            </Button>,
            <Button
              key="update"
              onClick={() => this.updateStudy()}
              disabled={!this.state.isUpdateStudy}
              style={{ color: this.state.isUpdateStudy ? '#f28227' : 'grey' }}
            >
              Update Study after Prediction
            </Button>,
            <Button key="stop" onClick={() => this.stop()} disabled={this.state.isUpdateStudy}>
              Stop Prediction
            </Button>,
            <Button key="clear" onClick={() => this.clearLogs()} disabled={this.state.isUpdateStudy}>
              Clear Output
            </Button>,
            <Button key="ok" onClick={() => this.handleClose()}>
              Close
            </Button>,
          ]}
        >
          <HelmetProvider>
            <div className="python-editor-container">
              {this.renderInterpreter()}
              <div className="predictionCodeDiv">
                <Helmet>
                  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/brython/3.7.1/brython.min.js" />
                  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/brython/3.7.1/brython_stdlib.js" />
                </Helmet>
                <Scripts code={code} style={{ backgroundColor: 'white' }} />
                <div className="python-editor-input">
                  <CodeEditor
                    className="python-code-editor"
                    value={code}
                    onChange={(text) => this.setState({ code: text })}
                    width={`${window.innerWidth / 2}px`}
                    height={`${window.innerHeight}px`}
                  ></CodeEditor>
                </div>
                <div className="python-editor-output">
                  <textarea className="python-output" readOnly value={output(outputArr)} placeholder="> output goes here..." ref={this.textLog} />
                </div>
              </div>
            </div>
          </HelmetProvider>
        </Modal>
      </div>
    );
  }
}
