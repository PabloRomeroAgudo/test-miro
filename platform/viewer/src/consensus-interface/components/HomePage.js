import React, { Component } from 'react';
import StudyDataManagement from './StudyDataManagement/studyDataManagement.js';
import PatientModeling from './PatientModeling/PatientModelingVTK.js';
import PlanEvaluation from './PatientModeling/PlanEvaluation/PlanEvaluationVTK';
import MainPage from './MainPage.js';
import Tooltip from '@mui/material/Tooltip';
import AiModelManagement from './AiModelIntegration/AiModelManagement';
import './styles.css';

class TabLinks extends React.Component {
  render() {
    let title = null;
    if (this.props.id == 'PatientModeling') {
      title = 'Visualization of the medical image data and the contours of ROIs';
    } else if (this.props.id == 'PlanEvaluation') {
      title = "Visualization of the patient's medical data, contours of ROIs, and the doses distribution of treatment plans";
    } else if (this.props.id == 'StudyDataManagement') {
      title = 'Managing uploaded studies';
    } else if (this.props.id == 'AiModelManagement') title = 'Managing AI models';
    return (
      <div>
        <Tooltip title={<h1 style={{ fontSize: '12px', width: 'fit-content', height: 'fit-content' }}>{title}</h1>} placement="bottom">
          <button
            className={this.props.active ? 'tablinks' + ' active' : 'tablinks'}
            id={this.props.id}
            onClick={() => {
              this.props.onClick();
            }}
            style={{ fontSize: '18px', paddingTop: '9px' }}
          >
            {' '}
            {this.props.value}
          </button>
        </Tooltip>
      </div>
    );
  }
}

export default class HomePage extends React.Component {
  constructor(props) {
    super(props);

    let activeTabID = 'StudyDataManagement';
    let StudyInstanceUID = null;

    if (this.props.StudyInstanceUID) {
      console.log('Starting application with images');

      activeTabID = 'PatientModeling';
      StudyInstanceUID = this.props.StudyInstanceUID;
    }

    this.state = {
      activeTabID,
      StudyInstanceUID: StudyInstanceUID,
      cleanDataSet: false,
    };
  }

  handleTabLinksClick(activeTabID) {
    this.setState({
      activeTabID: activeTabID,
    });
  }

  handlePatientSelection(StudyInstanceUID) {
    this.setState({
      StudyInstanceUID: StudyInstanceUID,
    });
  }

  activeTabContent() {
    switch (this.state.activeTabID) {
      case 'MainPage':
        return (
          <MainPage
            activeStudyInstanceUID={this.state.StudyInstanceUID}
            setStudy={(studyInstanceUID) => {
              this.handlePatientSelection(studyInstanceUID);
            }}
            setActiveTabID={(activeTabID) => {
              this.handleTabLinksClick(activeTabID);
            }}
          />
        );
        break;
      case 'StudyDataManagement':
        return (
          <StudyDataManagement
            activeStudyInstanceUID={this.state.StudyInstanceUID}
            setStudy={(studyInstanceUID) => {
              this.handlePatientSelection(studyInstanceUID);
            }}
            setActiveTabID={(activeTabID) => {
              this.handleTabLinksClick(activeTabID);
            }}
          />
        );
        break;
      case 'AiModelManagement':
        return (
          <AiModelManagement
            activeStudyInstanceUID={this.state.StudyInstanceUID}
            setStudy={(studyInstanceUID) => {
              this.handlePatientSelection(studyInstanceUID);
            }}
            setActiveTabID={(activeTabID) => {
              this.handleTabLinksClick(activeTabID);
            }}
          />
        );
        break;
      case 'PatientModeling':
        return (
          <PatientModeling
            activeStudyInstanceUID={this.state.StudyInstanceUID}
            setStudy={(studyInstanceUID) => {
              this.handlePatientSelection(studyInstanceUID);
            }}
            setActiveTabID={(activeTabID) => {
              this.handleTabLinksClick(activeTabID);
            }}
          />
        );
        break;
      case 'PlanEvaluation':
        return (
          <PlanEvaluation
            activeStudyInstanceUID={this.state.StudyInstanceUID}
            setStudy={(studyInstanceUID) => {
              this.handlePatientSelection(studyInstanceUID);
            }}
            setActiveTabID={(activeTabID) => {
              this.handleTabLinksClick(activeTabID);
            }}
          />
        );
        break;
      default:
        return <div> Error </div>;
    }
  }

  render() {
    const activeTabID = this.state.activeTabID;
    const tabContent = this.activeTabContent();
    return (
      <div className="mainBlock">
        <div className="tab">
          <TabLinks
            className="MainPage"
            id={'MainPage'}
            active={activeTabID === 'MainPage'}
            value={'Home'}
            onClick={() => {
              this.handleTabLinksClick('MainPage');
            }}
          />
          <TabLinks
            className="StudyManagement"
            id={'StudyDataManagement'}
            active={activeTabID === 'StudyDataManagement'}
            value={'Study Management'}
            onClick={() => {
              this.handleTabLinksClick('StudyDataManagement');
            }}
          />
          <TabLinks
            className="AiModelManagement"
            id={'AiModelManagement'}
            active={activeTabID === 'AiModelManagement'}
            value={'AI Model Management'}
            onClick={() => {
              this.handleTabLinksClick('AiModelManagement');
            }}
          />
          <TabLinks
            className="PatientModeling"
            id={'PatientModeling'}
            active={activeTabID === 'PatientModeling'}
            value={'Patient Modeling'}
            onClick={() => {
              this.handleTabLinksClick('PatientModeling');
            }}
          />
          <TabLinks
            className="PlanEvaluation"
            id={'PlanEvaluation'}
            active={activeTabID === 'PlanEvaluation'}
            value={'Plan Evaluation'}
            onClick={() => {
              this.handleTabLinksClick('PlanEvaluation');
            }}
          />
        </div>
        <div className="tabcontent">{tabContent}</div>
      </div>
    );
  }
}
