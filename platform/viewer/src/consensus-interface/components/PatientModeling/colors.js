
const fusionColors = [
  {
    name: 'None',
    id: 'None',
    primary: [0.5, 0.5, 0.5],
    secondary: [0.5, 0.5, 0.5],
  },
  {
    name: 'White/Pink',
    id: 'White/Pink',
    primary: [0.5, 0.5, 0.5],
    secondary: [255/255, 192/255, 203/255],
  },
  {
    name: 'Red/White',
    id: 'Red/White',
    primary: [1, 0, 0],
    secondary: [0.5, 0.5, 0.5],
  },
  {
    name: 'Blue/Orange',
    id: 'Blue/Orange',
    primary: [0, 0, 1],
    secondary: [1, 165/255, 0],
  },
  {
    name: 'Magenta/Green',
    id: 'Magenta/Green',
    primary: [1, 0, 1],
    secondary: [0, 1, 0],
  }
];

export default fusionColors;

