
import macro from '@kitware/vtk.js/macro';

function Overlay (publicAPI, model) {
  const scale = 1; //TODO
  
  model.svgContainer = document.createElement('svg');
  model.svgContainer.setAttribute(
    'style',
    'position: absolute; top: 0; left: 0; width: 100%; height: 100%;'
  );
  model.svgContainer.setAttribute('version', '1.1');
  model.svgContainer.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
  
  model.renderer = model.genericRenderWindow.getRenderer();
  model.renderWindow = model.genericRenderWindow.getRenderWindow();
  model.interactor = model.renderWindow.getInteractor();
  model.openGLRenderWindow = model.genericRenderWindow.getOpenGLRenderWindow();
  
  model.scale = scale;
    
  let [width, height] = model.openGLRenderWindow.getSize();
  width = width*scale;
  height = height*scale;

  model.container.appendChild(model.svgContainer);
  
  model.svgContainer.setAttribute(
   'viewBox',
    `0 0 ${width} ${height}`
  );
    
  model.svgContainer.setAttribute('width', `${width}`);
  model.svgContainer.setAttribute('height', `${height}`);
    
  model.node = document.createElement('t');
  model.node.setAttribute('width', `${width}`);
  model.node.setAttribute('height', `${height}`);
  
  model.svgContainer.appendChild(model.node);
 
  publicAPI.delete = () => {
    model.svgContainer.removeChild(model.node);
    model.container.removeChild(model.svgContainer);
    
    model.node = null;
    model.svgContainer = null;
    model.container = null;
    model.renderer = null;
    model.renderWindow = null;
    model.interactor = null;
    model.openGLRenderWindow = null;
  }
  
  publicAPI.setText = (upperLeft, upperRight, bottomLeft) => {
      if (!upperLeft)
        upperLeft = [];
      if (!upperRight)
        upperRight = [];
      if(!bottomLeft)
        bottomLeft = [];
      
      let [width, height] = model.openGLRenderWindow.getSize();
      width = width*scale;
      height = height*scale;
      
      const rightStyle = {textAlign: "right"};

      model.node.style.color = "#d19c4d";
      model.node.innerHTML = `
        <t id="container" style="fill: none; width: 100%; height: 100%;">
          <t style="position: absolute; width: 100%;  top:0; left: 0;">
            <table style="width: 100%;">
              <tr style="width: 100%;">
                <td>${upperLeft.length>0 ? upperLeft[0] : ""}</td><td style="text-align: right">${upperRight.length>0 ? upperRight[0] : ""}</td>
              </tr>
              <tr>
                <td>${upperLeft.length>1 ? upperLeft[1] : ""}</td><td style="text-align: right">${upperRight.length>1 ? upperRight[1] : ""}</td>
              </tr>
              <tr>
                <td>${upperLeft.length>2 ? upperLeft[2] : ""}</td><td style="text-align: right">${upperRight.length>2 ? upperRight[2] : ""}</td>
              </tr>
              <tr>
                <td>${upperLeft.length>3 ? upperLeft[3] : ""}</td><td style="text-align: right">${upperRight.length>3 ? upperRight[3] : ""}</td>
              </tr>
            </table>
          </t>
          <t style="position: absolute; width: 100%;  bottom:0; left: 0;">
            <table style="width: 100%; color: #ff0000;">
              <tr style="width: 100%;">
                <td>${bottomLeft.length>0 ? bottomLeft[0] : ""}</td><td style="text-align: right">${""}</td>
              </tr>
              <tr>
                <td>${bottomLeft.length>1 ? bottomLeft[1] : ""}</td><td style="text-align: right">${""}</td>
              </tr>
              <tr>
                <td>${bottomLeft.length>2 ? bottomLeft[2] : ""}</td><td style="text-align: right">${""}</td>
              </tr>
              <tr>
                <td>${bottomLeft.length>3 ? bottomLeft[3] : ""}</td><td style="text-align: right">${""}</td>
              </tr>
            </table>
          </t>
        </t>
            `;
  };
}
  
const DEFAULT_VALUES = {
};

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  macro.obj(publicAPI, model);
  macro.setGet(publicAPI, model, ['container', 'genericRenderWindow']);
  Overlay(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(extend, 'Overlay');

// ----------------------------------------------------------------------------

export default { newInstance, extend };

