import React from 'react';

import View2D from './View2D.js';
import View3D from './View3D.js';
import { Icon } from '../../utils/icons/Icon';

import './styles.css';

class TabLinks extends React.Component {
  render() {
    let buttonContent = null;

    switch (this.props.value) {
      case 'Normal screen':
        buttonContent = <Icon name="normalScreen" />;
        break;
      case 'Full screen':
        buttonContent = <Icon name="fullScreen" />;
        break;
      default:
        buttonContent = this.props.value;
    }

    return (
      <button
        className={this.props.active ? 'tablinks' + ' active' : 'tablinks'}
        id={this.props.id}
        onClick={() => {
          this.props.onClick();
        }}
      >
        {buttonContent}
      </button>
    );
  }
}

export default class VTKView extends React.Component {
  constructor(props) {
    super(props);

    let activeTabID = null;
    if (this.props.viewType == '3D') activeTabID = this.props.viewType;
    else activeTabID = this.props.sliceMode;

    this.state = {
      SeriesInstanceUID: this.props.SeriesInstanceUID,
      secondarySeriesInstanceUID: this.props.secondarySeriesInstanceUID,
      activeTabID,
      viewType: this.props.viewType, // '2D' or '3D'
      sliceMode: this.props.sliceMode, // only required if viewType=='2D'
    };
  }

  get2DRender() {
    return (
      <View2D
        className="view2D"
        SeriesInstanceUID={this.props.SeriesInstanceUID}
        secondarySeriesInstanceUID={this.props.secondarySeriesInstanceUID}
        imageDataSet={this.props.imageDataSet}
        segDataSet={this.props.segDataSet}
        doseDataSet={this.props.doseDataSet}
        doseIndex1={this.props.doseIndex1}
        doseIndex2={this.props.doseIndex2}
        sliceMode={this.state.sliceMode}
        doseToShow={this.props.doseToShow}
        doseColorSettings={this.props.doseColorSettings}
        colorbarFunctions={this.props.colorbarFunctions}
        doseScaleFactor={this.props.doseScaleFactor}
        viewName={this.props.viewName}
      />
    );
  }

  get3DRender() {
    return (
      <View3D
        SeriesInstanceUID={this.props.SeriesInstanceUID}
        secondarySeriesInstanceUID={this.props.secondarySeriesInstanceUID}
        imageDataSet={this.props.imageDataSet}
        segDataSet={this.props.segDataSet}
        doseDataSet={this.props.doseDataSet}
        doseIndex1={this.props.doseIndex1}
        doseIndex2={this.props.doseIndex2}
      />
    );
  }

  renderTitle() {
    if (this.props.title) {
      return <a className="doseViewTitle">{this.props.title}</a>;
    } else {
      return null;
    }
  }

  renderShowTabLink3D() {
    if (this.props.show3D == 'Yes') {
      return (
        <TabLinks
          id={'3D'}
          active={this.state.activeTabID === '3D'}
          value={'3D'}
          onClick={() => {
            this.handleTabLinksClick('3D');
          }}
        />
      );
    }
  }

  handleTabLinksClick(activeTabID) {
    switch (activeTabID) {
      case '3D':
        this.setState({ activeTabID, viewType: '3D' });
        break;
      case 'fullScreen':
        this.props.setFullScreen(this.state.sliceMode, !this.props.fullScreen);
        break;
      default:
        this.setState({ activeTabID, viewType: '2D', sliceMode: activeTabID });
    }
  }

  render() {
    let view;

    if (this.state.viewType == '2D') view = this.get2DRender();

    if (this.state.viewType == '3D') view = this.get3DRender();

    return (
      <div className="viewIJK3D">
        <div className="viewTab">
          <TabLinks
            id={'I'}
            active={this.state.activeTabID === 'I'}
            value={'Sagittal'}
            onClick={() => {
              this.handleTabLinksClick('I');
            }}
          />
          <TabLinks
            id={'J'}
            active={this.state.activeTabID === 'J'}
            value={'Coronal'}
            onClick={() => {
              this.handleTabLinksClick('J');
            }}
          />
          <TabLinks
            id={'K'}
            active={this.state.activeTabID === 'K'}
            value={'Axial'}
            onClick={() => {
              this.handleTabLinksClick('K');
            }}
          />
          {/* <TabLinks id={"3D"} active={this.state.activeTabID==="3D"} value={"3D"} onClick={() => {this.handleTabLinksClick("3D")}}/> */}
          {this.renderShowTabLink3D()}
          <TabLinks
            id={'fullScreen'}
            active={this.state.activeTabID != '3D' && this.state.fullScreen}
            value={this.props.fullScreen ? 'Normal screen' : 'Full screen'}
            onClick={() => {
              this.handleTabLinksClick('fullScreen');
            }}
          />
          {this.renderTitle()}
        </div>
        <div className="viewTabContent">{view}</div>
      </div>
    );
  }
}
