import WindowWLSettings from './WindowWLSettings';
import PaintSettings from './PaintSettings';
import LayoutSettings from './LayoutSettings';

export {WindowWLSettings, PaintSettings, LayoutSettings};
