import React from 'react';

import ViewsApi from '../Services/ViewsApi.js';
import DataSet from '../../../data/DataSet.js';
import { Icon } from '../../../utils/icons/Icon';

import '../PlanEvaluation/style/styles.css';

export default class DoseToolBar extends React.Component {
  constructor(props) {
    super(props);

    this.api = {
      endAll: () => this.endAll(),
      endCrossHairs: () => this.endCrossHairs(),
      startCrossHairs: () => this.startCrossHairs(),
    };

    this.dataSet = DataSet.getInstance();
    this.viewApi = ViewsApi.getInstance();

    this.state = {
      currentColorID: 'None',
      activeTool: 'crossHairs',
      settingWindow: null,
    };

    this.viewApi.setActiveWidget('crossHairs'); // CrossHairs are launched directly in view2D
  }

  /* TOOLS *
   *********/
  closeSettingWindow() {
    this.setState({ settingWindow: null });
  }

  endAll() {
    switch (this.state.activeTool) {
      case 'crossHairs':
        this.endCrossHairs();
        break;
      default:
    }
    this.setState({ activeTool: null });
  }

  endCrossHairs() {
    this.viewApi.endCrossHairs();

    this.viewApi.setActiveWidget(null);
    this.setState({ activeTool: this.viewApi.getActiveWidget() });
  }

  startCrossHairs() {
    if (this.state.activeTool === 'crossHairs') {
      this.endCrossHairs();
      return;
    } else this.endAll();

    if (this.viewApi.startCrossHairs()) {
      this.viewApi.setActiveWidget('crossHairs');
      this.setState({
        activeTool: this.viewApi.getActiveWidget(),
      });
    }
  }

  toggleSettingsWindow() {
    let windowSetting = null;
    switch (this.props.activeTool) {
      case 'crossHairs':
        break;
      default:
        windowSetting = null;
        break;
    }
    if (windowSetting !== null) {
      this.setState({ settingWindow: windowSetting });
    }
  }

  /* MAIN *
   ********/
  activeTabContent() {
    return <DoseTools api={this.api} />;
  }

  render() {
    switch (this.props.activeTool) {
      case 'crosshairs':
        break;
      default:
    }
    return (
      <div className="DoseToolBar">
        <div className="crossHaires" style={{ display: 'inline-flex', marginTop: '12px', alignItems: 'center' }}>
          <Icon className="smallIcon" name="crosshairs" />
          <div className="crossHarisTd" onClick={() => this.startCrossHairs()} style={{ marginLeft: '5px' }}>
            Crosshairs
          </div>
        </div>
        {this.state.settingWindow}
      </div>
    );
  }
}
