import React from 'react';
import PropTypes from 'prop-types';
import '../../styles.css';

function toStringColor(colorData) {
  let color = 'rgb(' + colorData.toString() + ')';
  return color;
}

function emptyToZeroValue(value) {
  if (value == 'NaN' || !value) {
    return '0.00';
  } else {
    return value;
  }
}

DoseStatisticTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default function DoseStatisticTable({ data }) {
  const result = (
    <table
      className="doseStatisticTable"
      style={{
        border: 'solid 1px grey',
        fontSize: '14px',
        padding: '2px',
        textAlign: 'center',
      }}
    >
      <thead>
        <tr>
          <th
            className="doseNameHeader"
            rowSpan="3"
            style={{
              border: 'solid 1px black',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            Dose
          </th>
          <th
            className="roiHeader"
            rowSpan="3"
            style={{
              border: 'solid 1px black',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            ROI
          </th>
          <th
            className="roiVolHeader"
            rowSpan="3"
            style={{
              border: 'solid 1px black',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            ROI vol.[cm^3]
          </th>
          <th
            className="doseValues"
            colSpan="7"
            style={{
              border: 'solid 1px black',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            Dose [Gy]
          </th>
          <th
            className="volumes"
            colSpan="5"
            style={{
              border: 'solid 1px black',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            Volume [%]
          </th>
        </tr>
        <tr>
          <th
            className="doseD99Header"
            style={{
              border: 'solid 1px grey',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            D99
          </th>
          <th
            className="doseD98Header"
            style={{
              border: 'solid 1px grey',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            D98
          </th>
          <th
            className="doseD95Header"
            style={{
              border: 'solid 1px grey',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            D95
          </th>
          <th
            className="averageHeader"
            style={{
              border: 'solid 1px grey',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            Avg
          </th>
          <th
            className="doseD50Header"
            style={{
              border: 'solid 1px grey',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            D50
          </th>
          <th
            className="doseD5Header"
            style={{
              border: 'solid 1px grey',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            D5
          </th>
          <th
            className="doseD2Header"
            style={{
              border: 'solid 1px grey',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            D2
          </th>
          <th
            className="volumeV40Header"
            style={{
              border: 'solid 1px grey',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            V40Gy
          </th>
          <th
            className="volumeV30Header"
            style={{
              border: 'solid 1px grey',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            V30Gy
          </th>
          <th
            className="volumeV20Header"
            style={{
              border: 'solid 1px grey',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            V20Gy
          </th>
          <th
            className="volumeV10Header"
            style={{
              border: 'solid 1px grey',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            V10Gy
          </th>
          <th
            className="volumeV5Header"
            style={{
              border: 'solid 1px grey',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            V5Gy
          </th>
        </tr>
      </thead>
      <tbody className="doseStatistcTableBody">
        {data.map((item) => (
          // eslint-disable-next-line react/jsx-key
          <tr className="doseStatisticTableTR">
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'left',
              }}
            >
              <div className="doseStatisticDoseNameDiv">
                <span
                  className="doseName"
                  style={{
                    textAlign: 'left',
                  }}
                >
                  {item.general.doseName}
                </span>
              </div>
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              <div className="doseStatisticRoiNameDiv">
                <div
                  className={'roiColor'}
                  style={{
                    width: '14px',
                    height: '14px',
                    backgroundColor: toStringColor(item.general.color),
                    marginRight: '4px',
                    marginLeft: '4px',
                  }}
                ></div>
                <span
                  className="roiName"
                  style={{
                    textAlign: 'left',
                  }}
                >
                  {item.general.roi}
                </span>
              </div>
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {item.general.roiVol}
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {emptyToZeroValue(item.dose.doseD99)}
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {emptyToZeroValue(item.dose.doseD98)}
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {emptyToZeroValue(item.dose.doseD95)}
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {emptyToZeroValue(item.dose.average)}
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {emptyToZeroValue(item.dose.doseD50)}
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {emptyToZeroValue(item.dose.doseD5)}
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {emptyToZeroValue(item.dose.doseD2)}
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {emptyToZeroValue(item.volume.volumeV40Gy)}
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {emptyToZeroValue(item.volume.volumeV30Gy)}
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {emptyToZeroValue(item.volume.volumeV20Gy)}
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {emptyToZeroValue(item.volume.volumeV10Gy)}
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {emptyToZeroValue(item.volume.volumeV5Gy)}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
  return result;
}
