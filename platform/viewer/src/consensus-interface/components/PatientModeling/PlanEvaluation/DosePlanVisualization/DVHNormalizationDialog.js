import { Button, Modal, Dropdown, Menu, Space } from 'antd';
import React, { Component } from 'react';
import '../style/dvhSettingDialogStyles.css';

const dvhMetrics = ['D99', 'D98', 'D95', 'D90', 'D50', 'D5', 'D2', 'Dmean', 'Dmax', 'V5', 'V10', 'V20', 'V30', 'V40'];

export default class DVHNormalizationDialog extends React.Component {
  constructor(props) {
    super(props);
    props.dvhSettingFunctions.setVisible = (isVisible) => this.setVisible(isVisible);
    this.onContourValueChange = this.onContourValueChange.bind(this);
    this.onDVHMetricChange = this.onDVHMetricChange.bind(this);

    this.state = {
      visible: false,
      selectedContour: props.dvhNormalizationSetting.selectedContour,
      selectedDVHMetric: props.dvhNormalizationSetting.selectedDVHMetric,
      prescriptionValue: props.dvhNormalizationSetting.prescriptionValue,
      enterPrescriptionValue: props.dvhNormalizationSetting.prescriptionValue,
    };
  }

  componentDidUpdate(prevProps) {
    this.props.dvhSettingFunctions.setVisible = (isVisible) => this.setVisible(isVisible);
  }

  setVisible(isVisible) {
    this.setState({
      visible: isVisible,
    });
  }

  handleOk() {
    if (this.state.selectedContour != null && this.state.selectedDVHMetric != null) {
      this.props.onDvhSettingChanged({
        selectedContour: this.state.selectedContour,
        selectedDVHMetric: this.state.selectedDVHMetric,
        prescriptionValue: this.state.prescriptionValue,
      });
      this.spinnerStatus = false;
      this.setVisible(false);
    } else {
      if (
        this.state.selectedContour == null ||
        (this.state.selectedContour != null && this.state.selectedContour.includes('no normalization')) ||
        this.state.selectedDVHMetric == null
      ) {
        this.setState({
          showMessage: true,
        });
      }
    }
  }

  handleCancel() {
    this.setState({
      selectedContour: this.props.dvhNormalizationSetting.selectedContour,
      selectedDVHMetric: this.props.dvhNormalizationSetting.selectedDVHMetric,
      prescriptionValue: this.props.dvhNormalizationSetting.prescriptionValue,
      enterPrescriptionValue: this.props.dvhNormalizationSetting.prescriptionValue,
      showMessage: false,
    });
    this.setVisible(false);
  }

  renderErrorMessage() {
    let message = 'Please select the contour';
    return this.state.showMessage ? (
      <tr className="showNomalizationErrorMessageTR">
        <td className="messageLabel" style={{ width: '100px' }}>
          Message:
        </td>
        <td className="messageText">
          <h3 className="showMessageH3" style={{ color: 'red', fontSize: '15px' }}>
            {message}
          </h3>
        </td>
      </tr>
    ) : null;
  }

  handleClear() {
    this.props.onDvhSettingChanged({
      selectedContour: null,
      selectedDVHMetric: this.state.selectedDVHMetric,
      prescriptionValue: this.state.prescriptionValue,
    });
    this.setVisible(false);
  }

  onContourValueChange(newValue) {
    this.setState({
      selectedContour: newValue,
    });
  }

  onDVHMetricChange(newValue) {
    this.setState({
      selectedDVHMetric: newValue,
    });
  }

  onPrescriptionValueChange(event) {
    const newValue = parseFloat(event.target.value);
    if (isNaN(event.target.value) || isNaN(newValue) || newValue < 0) {
      // not a number
      event.target.style.backgroundColor = 'red';
    } else {
      // is number
      event.target.style.backgroundColor = 'white';
      this.setState({
        prescriptionValue: newValue,
      });
    }
    this.setState({
      enterPrescriptionValue: event.target.value,
    });
  }

  renderContourSelectDropDown(segs) {
    const menu = (
      <div className="segDropdownItemDiv">
        <Menu
          items={segs.map((item, index) => {
            return {
              key: index,
              label: <a style={{ color: 'black' }}>{item.getLabel()}</a>,
            };
          })}
          onClick={({ key }) => this.onContourValueChange(segs[key])}
        />
      </div>
    );
    return (
      <Space direction="vertical">
        <Space wrap>
          <Dropdown overlay={menu} placement="bottom">
            <Button
              style={{
                backgroundColor: 'white',
                color: 'black',
                border: '1px solid grey',
                borderRadius: '2px',
                width: '170px',
              }}
            >
              {this.state.selectedContour == null ? '---- no normalization ----' : this.state.selectedContour.getLabel()}
            </Button>
          </Dropdown>
        </Space>
      </Space>
    );
  }

  renderMetricSelectDropDown() {
    const menu = (
      <div className="dvhMetricDropdownItemDiv">
        <Menu
          items={dvhMetrics.map((item, index) => {
            return {
              key: index,
              label: <a style={{ color: 'black' }}>{item}</a>,
            };
          })}
          onClick={({ key }) => this.onDVHMetricChange(dvhMetrics[key])}
        />
      </div>
    );
    return (
      <Space direction="vertical">
        <Space wrap>
          <Dropdown overlay={menu} placement="bottom">
            <Button
              style={{
                backgroundColor: 'white',
                color: 'black',
                border: '1px solid grey',
                borderRadius: '2px',
                width: '170px',
              }}
            >
              {this.state.selectedDVHMetric}
            </Button>
          </Dropdown>
        </Space>
      </Space>
    );
  }

  renderSettingContent() {
    const segs = this.props.segDataSet.getSegs();
    return (
      <div>
        <h2 className="dvhNormalizationDialogTitle">Normalization</h2>
        <table className="dvhNormalizationTable">
          <tbody className="dvhNormalizationTbody">
            <tr className="dvhNormalizationTR">
              <td className="contourLabel">Contour:</td>
              <td className="dvhNormalizationTd">{this.renderContourSelectDropDown(segs)}</td>
              <td className="dvhMetricLabel">DVH metric:</td>
              <td className="dvhMetricTd">{this.renderMetricSelectDropDown()}</td>
            </tr>
            <tr className="dvhPrescriptionLabelTR">
              <td className="prescriptionLabel">Prescription [Gy]:</td>
              <td>
                <input
                  className="prescriptionValueInput"
                  type="text"
                  id="inputPrescriptionValueId"
                  size="10"
                  onChange={(event) => this.onPrescriptionValueChange(event)}
                  value={this.state.enterPrescriptionValue}
                ></input>
              </td>
            </tr>
            {this.renderErrorMessage()}
          </tbody>
        </table>
      </div>
    );
  }

  render() {
    // console.log("********** render dvh setting");
    return (
      <Modal
        className="dvhNormalizationModal"
        destroyOnClose={true}
        visible={this.state.visible}
        onOk={() => this.handleOk()}
        onCancel={() => this.handleCancel()}
        footer={[
          <Button key="cancel" onClick={() => this.handleCancel()}>
            Cancel
          </Button>,
          <Button key="clear" onClick={() => this.handleClear()}>
            Clear
          </Button>,
          <Button key="ok" type="primary" onClick={() => this.handleOk()}>
            OK
          </Button>,
        ]}
      >
        {this.renderSettingContent()}
      </Modal>
    );
  }
}
