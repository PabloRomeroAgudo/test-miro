import React from 'react';
import PropTypes from 'prop-types';
import '../../styles.css';

function toStringColor(colorData) {
  let color = 'rgb(' + colorData.toString() + ')';
  return color;
}

function emptyToZeroValue(value) {
  if (value == 'NaNGy' || !value) {
    return '0.00Gy';
  } else {
    return value;
  }
}

function getGoalStatus(doseNumber, item) {
  const value = item.values.length > doseNumber ? item.values[doseNumber] : '---';
  const passed = item.passed.length > doseNumber ? item.passed[doseNumber] : false;
  const borderColor = passed ? '#AEF887' : '#990000';
  const bgColor = passed ? '#2C735C' : '#FF0000';

  return (
    <td
      style={{
        padding: '1px',
        border: 'solid 1px grey',
        color: 'white',
        fontSize: '12px',
        textAlign: 'center',
        wordBreak: 'break-word',
      }}
    >
      <div
        style={{
          border: 'solid 2px ' + borderColor,
          borderRadius: '7px',
          backgroundColor: bgColor,
        }}
      >
        {emptyToZeroValue(value)}
      </div>
    </td>
  );
}

ClinicalGoalsTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.string).isRequired,
};

function ClinicalGoalsTable({ data }) {
  const result = (
    <table
      className="clinicalGoalsTable"
      style={{
        border: 'solid 1px grey',
        fontSize: '14px',
        padding: '2px',
        textAlign: 'center',
      }}
    >
      <thead>
        <tr>
          <th
            className="priorityHeader"
            rowSpan="1"
            style={{
              border: 'solid 1px black',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            Priority
          </th>
          <th
            className="roiPoiHeader"
            style={{
              border: 'solid 1px black',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            ROI/POI
          </th>
          <th
            className="cgHeader"
            style={{
              border: 'solid 1px black',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            Clinical Goals
          </th>
          <th
            className="dose1valueHeader"
            style={{
              border: 'solid 1px black',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            Dose 1
          </th>
          <th
            className="dose2valueHeader"
            style={{
              border: 'solid 1px black',
              textAlign: 'center',
              fontSize: '12px',
              padding: '2px',
              backgroundColor: 'grey',
              color: 'white',
            }}
          >
            Dose 2
          </th>
        </tr>
      </thead>
      <tbody className="clinicalGoalsTBody">
        {data.map((item) => (
          // eslint-disable-next-line react/jsx-key
          <tr className="clinicalGoalsTableTR">
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'left',
              }}
            >
              <div className="clinicalGoalsRoiPoiDiv">
                <span
                  className="priority"
                  style={{
                    textAlign: 'left',
                  }}
                >
                  {item.priority}
                </span>
              </div>
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              <div className="clinicalGoalsRoiPoiDiv">
                <div
                  className={'roiPoiColor'}
                  style={{
                    width: '14px',
                    height: '14px',
                    backgroundColor: toStringColor(item.color),
                    marginRight: '4px',
                    marginLeft: '4px',
                  }}
                ></div>
                <span
                  className="roiPoiName"
                  style={{
                    textAlign: 'left',
                  }}
                >
                  {item.roiPoi}
                </span>
              </div>
            </td>
            <td
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                color: 'white',
                fontSize: '12px',
                textAlign: 'center',
              }}
            >
              {item.goal}
            </td>
            {getGoalStatus(0, item)}
            {getGoalStatus(1, item)}
          </tr>
        ))}
      </tbody>
    </table>
  );
  return result;
}
export default ClinicalGoalsTable;
