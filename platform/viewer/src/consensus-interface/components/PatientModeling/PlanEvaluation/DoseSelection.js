import React, { Component } from "react"
import uniqBy from 'lodash/uniqBy'
import {SETTING_NAMES, MainSettings} from '../../../utils/MainSettings'
import '../PlanEvaluation/style/styles.css'

export default class DoseSelection extends React.Component {
    constructor(props) {
        super(props)

        const study = this.props.study
        let activeRTDoseSeriesUID = null
        let activeRTDoseSOPUID = null

        if (study) {
            activeRTDoseSeriesUID = uniqBy(study, 'SeriesInstanceUID').filter(study => (study.modalities=="RTDOSE")).map(study => study.SeriesInstanceUID)[0];
            activeRTDoseSOPUID = study.filter(study => (study.SeriesInstanceUID==activeRTDoseSeriesUID)).map(study => study.SOPInstanceUID)[0];
        }

        this.state = {
            activeRTDoseSeriesUID,
            activeRTDoseSOPUID,
            onSelectedRTDose: this.props.onSelectedRTDose,
            study,
        }

        if (study) {
            this.props.onSelectedRTDose(activeRTDoseSeriesUID, activeRTDoseSOPUID);
        }
    }

    getRTDoseUIDs() {
        if (! this.state.study)
          return null;
        
        return uniqBy(this.state.study, 'SeriesInstanceUID').filter(study => (study.modalities=="RTDOSE")).map(study => study.SeriesInstanceUID);
    }

    getRTDoseNames() {
        if (! this.state.study)
          return null;
        
        return uniqBy(this.state.study, 'SeriesInstanceUID').filter(study => (study.modalities=="RTDOSE")).map(study => {
          return ({
            SeriesDescription: study.SeriesDescription,
            SeriesInstanceUID: study.SeriesInstanceUID,
          });
        });
    }

    handleChangeActiveDose(event) {
        const activeRTDoseSOPUID = this.state.study.filter(study => (study.SeriesInstanceUID==event.target.value)).map(study => study.SOPInstanceUID);
        this.state.onSelectedRTDose(event.target.value, activeRTDoseSOPUID[0]);
        this.setState({activeRTDoseSeriesUID: event.target.value, activeRTDoseSOPUID:activeRTDoseSOPUID[0]});
    };

    render() {
        const DoseNames = this.getRTDoseNames();
        
        if (!DoseNames || !DoseNames.length)
          return (
            <div>No Contour Found</div>
          );
          
       const DoseNamesOptions = DoseNames.map(dose => {
          return (
            <option key={dose.SeriesInstanceUID} value={dose.SeriesInstanceUID}>
              {dose.SeriesDescription}
            </option>
          );
        });
        
        const settings = new MainSettings();
        const pacsURL = settings.get(SETTING_NAMES.PACS_URL);
        
        return (
          <div className="DoseSelection">
            <select
              id="selectDose"
              value={this.state.activeRTDoseSeriesUID}
              onChange={(event) => {this.handleChangeActiveDose(event)}}>
              {DoseNamesOptions}
            </select>
          </div>
        );
      }
}