import { Button, Modal } from 'antd';
import React from 'react';
import { LinearProgress } from '@mui/material';
import { ImFolderDownload } from 'react-icons/im';
import { FiDelete } from 'react-icons/fi';
import ContourModificationHistoryDataSet from '../../data/ContourModificationHistoryDataSet';
import './styles.css';

export class StudyContentTable extends React.Component {
  constructor(props) {
    super(props);
  }

  renderTableHeader(headerName, width) {
    return (
      <th
        className={'studyManagement' + headerName + 'TH'}
        rowSpan="1"
        style={{
          border: 'solid 1px black',
          textAlign: 'center',
          fontSize: '12px',
          padding: '2px',
          background: 'grey',
          color: 'white',
          width: width,
        }}
      >
        {headerName}
      </th>
    );
  }

  checkDataRowStyle(data, width) {
    const style = {
      padding: '1px',
      border: 'solid 1px grey',
      fontSize: '14px',
      textAlign: 'left',
      width: width,
    };
    const ctStyle = {
      padding: '1px',
      border: 'solid 1px grey',
      fontSize: '17px',
      textAlign: 'left',
      width: width,
      fontWeight: 'bold',
      fontStyle: 'italic',
      color: '#F28227',
    };
    const seriesDescriptionStyle = {
      padding: '1px',
      border: 'solid 1px grey',
      fontSize: '15px',
      textAlign: 'left',
      width: width,
      fontWeight: 'bold',
      fontStyle: 'italic',
      color: '#F28227',
    };
    if (data.name.toUpperCase() == 'MODALITY') {
      return ctStyle;
    } else if (data.name.toUpperCase() == 'SERIESDESCRIPTION') {
      return seriesDescriptionStyle;
    } else {
      return style;
    }
  }

  clickDeleteSelectedDicomFile(instanceID) {
    this.props.onClickDeleteDicomFile(instanceID);
  }

  renderDataRow(data) {
    if (data.name.toUpperCase() == 'MODALITY' && data.value) {
      const instanceID = data.value.instanceID;
      const href = instanceID ? '/downloaddicom/' + instanceID + '/file' : '';
      return data.value.modalities != 'CT' ? (
        <div>
          {data.value.modalities}
          <div
            className="downloadFoloderDiv"
            style={{
              display: 'inline-flex',
              paddingLeft: '40px',
            }}
          >
            <ImFolderDownload
              className="downloadSingleFileButton"
              style={{
                fontSize: '23px',
                textAlign: 'right',
                color: '#9CCEF9',
                paddingTop: '2px',
                paddingBottom: '-5px',
                paddingRight: '4px',
              }}
            ></ImFolderDownload>
            <a href={href} style={{ color: '#9CCEF9' }} download>
              Download
            </a>
            <FiDelete
              className="deleteSingleFileButton"
              style={{
                color: 'red',
                fontSize: '23px',
                marginLeft: '40px',
                paddingTop: '5px',
              }}
              onClick={() => this.clickDeleteSelectedDicomFile(instanceID)}
            ></FiDelete>
          </div>
        </div>
      ) : (
        <div>{data.value.modalities}</div>
      );
    } else {
      return data.value;
    }
  }

  renderTableTBody() {
    return (
      <tbody className="studyContentTableTBody">
        {this.props.metaData.map((item) => (
          <tr className="studyContentTR">
            <td style={this.checkDataRowStyle(item, '30%')}>{item.name}</td>
            <td style={this.checkDataRowStyle(item, '70%')}>{this.renderDataRow(item)}</td>
          </tr>
        ))}
      </tbody>
    );
  }

  render() {
    return (
      <table
        className="studyContentTable"
        style={{
          border: 'sold 1px grey',
          fontSize: '14px',
          padding: '2px',
          textAlign: 'left',
          borderCollapse: 'inherit',
          width: '100%',
          paddingBottom: '10px',
        }}
      >
        <thead>
          <tr className="studyContentTableHeaderTR">
            {this.renderTableHeader('Name', '30%')}
            {this.renderTableHeader('Value', '70%')}
          </tr>
        </thead>
        {this.renderTableTBody()}
      </table>
    );
  }
}

export default class StudyContentDialog extends React.Component {
  constructor(props) {
    super(props);
    props.studyContentDialogFunction.setVisible = (isVisible) => this.setVisible(isVisible);

    this.state = {
      visible: false,
      href: null,
      isDownloading: false,
      isConfirmingDeleteDicomFile: false,
      deleteDicomFileIid: null,
      isDeleting: false,
      needReloadActiveStudy: false,
    };

    this._isMounted = false;
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate(prevProps) {
    this.props.studyContentDialogFunction.setVisible = (isVisible) => this.setVisible(isVisible);
    if (prevProps.openedStudyContentUID != this.props.openedStudyContentUID) {
      this.getStudyUID(this.props.openedStudyContentUID).then((id) => {
        this.setState({
          href: '/downloadstudy/' + id + '/archive',
        });
      });
    }
  }

  setVisible(isVisible) {
    this.setState({
      visible: isVisible,
    });
  }

  handleOk() {
    this.setVisible(false);
  }

  async getStudyUID(siuid) {
    const listOfStudyIDs = await (await fetch('/getstudy', { method: 'GET' })).json();
    for (var i = 0; i < listOfStudyIDs.length; i++) {
      const studyInfo = await (await fetch('/getstudy/' + listOfStudyIDs[i], { method: 'GET' })).json();
      if (studyInfo.MainDicomTags.StudyInstanceUID == siuid) {
        const uid = listOfStudyIDs[i].toString();
        return uid;
      }
    }
  }

  handleConfirmedDeleteDicomFile() {
    const sid = this.state.deleteDicomFileIid;
    this.setState({
      isDeleting: true,
    });

    // delete in server
    this.deleteDicomFile(sid).finally(() => {
      this.setState({
        isDeleting: false,
        visible: false,
      });
      if (this.state.needReloadActiveStudy && this.props.openedStudyContentUID == this.props.activeStudy) {
        this.props.onReloadActiveStudy();
      }
    });
  }

  async deleteDicomFile(instanceID) {
    await fetch('/deleteDicomFile/' + instanceID, { method: 'DELETE' });
  }

  handleClickDeleteDicomFile(instanceID) {
    this.setState({
      isConfirmingDeleteDicomFile: true,
      deleteDicomFileIid: instanceID,
      needReloadActiveStudy: true,
    });
  }

  renderImageMetaData(metaData, modality) {
    return metaData ? (
      metaData.map((data) => (
        <StudyContentTable
          className={modality + '_ctContentTable'}
          metaData={data}
          onClickDeleteDicomFile={(instanceID, event) => this.deleteSelectedDicomFile(instanceID)}
        />
      ))
    ) : (
      <div></div>
    );
  }

  renderPlanMetaData(planMetaData) {
    return planMetaData ? (
      planMetaData.map((data) => (
        <StudyContentTable
          className="planContentTable"
          metaData={data}
          openedStudyContentUID={this.props.openedStudyContentUID}
          onClickDeleteDicomFile={(instanceID, event) => this.handleClickDeleteDicomFile(instanceID, event)}
        />
      ))
    ) : (
      <div></div>
    );
  }

  renderRegMetaData(regMetaData) {
    return regMetaData ? (
      regMetaData.map((data) => (
        <StudyContentTable
          className="regContentTable"
          metaData={data}
          openedStudyContentUID={this.props.openedStudyContentUID}
          onClickDeleteDicomFile={(instanceID, event) => this.handleClickDeleteDicomFile(instanceID, event)}
        />
      ))
    ) : (
      <div></div>
    );
  }

  renderRTStructureMetaData(rtStructureMetaData) {
    return rtStructureMetaData ? (
      rtStructureMetaData.map((rtsData) => (
        <StudyContentTable
          className="rtStructureContentTable"
          metaData={rtsData}
          openedStudyContentUID={this.props.openedStudyContentUID}
          onClickDeleteDicomFile={(instanceID, event) => this.handleClickDeleteDicomFile(instanceID, event)}
        ></StudyContentTable>
      ))
    ) : (
      <div></div>
    );
  }

  renderDoseMetaData(doseMetaData) {
    return doseMetaData ? (
      doseMetaData.map((dose) => (
        <StudyContentTable
          className={'doseContentTable'}
          metaData={dose}
          openedStudyContentUID={this.props.openedStudyContentUID}
          onClickDeleteDicomFile={(sUID, event) => this.handleClickDeleteDicomFile(sUID, event)}
        />
      ))
    ) : (
      <div></div>
    );
  }

  getItemModalityValue(item) {
    let result = { modalities: item.modalities, instanceID: null };
    for (let i = 0; i < this.props.studyMetaData.downloadInfoData.length; i++) {
      const seriesIUID = this.props.studyMetaData.downloadInfoData[i].seriesIUID.toUpperCase();
      if (
        item.modalities.toUpperCase() == 'RTDOSE' ||
        item.modalities.toUpperCase() == 'RTSTRUCT' ||
        item.modalities.toUpperCase() == 'RTPLAN' ||
        item.modalities.toUpperCase() == 'MR' ||
        item.modalities.toUpperCase() == 'REG' ||
        item.modalities.toUpperCase() == 'PT'
      ) {
        if (item.SeriesInstanceUID.toUpperCase() == seriesIUID) {
          result = { modalities: item.modalities, instanceID: this.props.studyMetaData.downloadInfoData[i].instanceID };
          break;
        }
      }
    }
    return result;
  }

  parseMetaData() {
    const ctMetaData = [];
    const planMetaData = [];
    const rtStructureMetaData = [];
    const doseMetaData = [];
    const regMetaData = [];
    const ptMetaData = [];
    const mrMetaData = [];

    let ctNumber = 0;
    let ctItem = null;
    let mrItem = null;
    let mrNumber = 0;
    let ptItem = null;
    let ptNumber = 0;
    if (this.props.studyMetaData) {
      for (let i = 0; i < this.props.studyMetaData.metaData.length; i++) {
        let item = this.props.studyMetaData.metaData[i];
        if (item.modalities == 'CT') {
          ctNumber++;
          ctItem = item;
        } else if (item.modalities == 'MR') {
          mrNumber++;
          mrItem = item;
        } else if (item.modalities == 'PT') {
          ptNumber++;
          ptItem = item;
        } else if (item.modalities == 'RTPLAN') {
          let planItem = [
            { name: 'Modality', value: this.getItemModalityValue(item) },
            { name: 'SeriesDescription', value: item.SeriesDescription },
            { name: 'SeriesInstanceUID', value: item.SeriesInstanceUID },
            { name: 'PatientID', value: item.PatientID },
            { name: 'StudyDateTime', value: item.StudyDate + ' ' + item.StudyTime },
            { name: 'SeriesTime', value: item.SeriesTime },
            { name: 'Manufacturer', value: item.Manufacturer },
            { name: 'InstanceCreationDate', value: item.InstanceCreationDate },
          ];
          planMetaData.push(planItem);
        } else if (item.modalities == 'RTSTRUCT') {
          let rtStructureItem = [
            { name: 'Modality', value: this.getItemModalityValue(item) },
            { name: 'SeriesDescription', value: item.SeriesDescription },
            { name: 'SeriesInstanceUID', value: item.SeriesInstanceUID },
            { name: 'StudyDateTime', value: item.StudyDate + ' ' + item.StudyTime },
            { name: 'SeriesTime', value: item.SeriesTime },
            { name: 'Manufacturer', value: item.Manufacturer },
            { name: 'InstanceCreationDate', value: item.StructureSetLabel },
            { name: 'InstanceNumber', value: item.InstanceNumber },
          ];
          rtStructureMetaData.push(rtStructureItem);
        } else if (item.modalities == 'RTDOSE') {
          let doseDataItem = [
            { name: 'Modality', value: this.getItemModalityValue(item) },
            { name: 'StudyDescription', value: item.StudyDescription },
            { name: 'SeriesDescription', value: item.SeriesDescription },
            { name: 'SeriesInstanceUID', value: item.SeriesInstanceUID },
            { name: 'ImageOrientationPatient', value: item.ImageOrientationPatient },
            { name: 'AccessionNumber', value: item.AccessionNumber },
            { name: 'InstanceCreationDate', value: item.InstanceCreationDate },
            { name: 'Manufacturer', value: item.Manufacturer },
            { name: 'StudyDateTime', value: item.StudyDate + ' ' + item.StudyTime },
            { name: 'SliceThickness', value: item.SliceThickness },
            { name: 'InstanceNumber', value: item.InstanceNumber },
            { name: 'PixelSpacing', value: item.PixelSpacing },
            { name: 'Rows', value: item.Rows },
          ];
          doseMetaData.push(doseDataItem);
        } else if (item.modalities == 'REG') {
          let regDataItem = [
            { name: 'Modality', value: this.getItemModalityValue(item) },
            { name: 'StudyDescription', value: item.StudyDescription },
            { name: 'SeriesDescription', value: item.SeriesDescription },
            { name: 'AccessionNumber', value: item.AccessionNumber },
            { name: 'InstanceCreationDate', value: item.InstanceCreationDate },
            { name: 'Manufacturer', value: item.Manufacturer },
            { name: 'StudyDateTime', value: item.StudyDate + ' ' + item.StudyTime },
            { name: 'InstanceNumber', value: item.InstanceNumber },
            { name: 'RetrieveAETitle', value: item.RetrieveAETitle },
            { name: 'QueryRetrieveLevel', value: item.QueryRetrieveLevel },
            { name: 'DeviceSerialNumber', value: item.DeviceSerialNumber },
          ];
          regMetaData.push(regDataItem);
        }
      }

      // add only one CT information
      if (ctItem) {
        let readCtItem = [
          { name: 'Modality', value: { modalities: ctItem.modalities, seriesIUID: null } },
          { name: 'StudyDateTime', value: ctItem.StudyDate + ' ' + ctItem.StudyTime },
          { name: 'SeriesInstanceUID', value: ctItem.SeriesInstanceUID },
          { name: 'Manufacturer', value: ctItem.Manufacturer },
          { name: 'BitsAllocated', value: ctItem.BitsAllocated },
          { name: 'Columns', value: ctItem.Columns },
          { name: 'ImageOrientationPatient', value: ctItem.ImageOrientationPatient },
          { name: 'ImagePositionPatient', value: ctItem.ImagePositionPatient },
          { name: 'PatientPosition', value: ctItem.PatientPosition },
          { name: 'Rows', value: ctItem.Rows },
          { name: 'Columns', value: ctItem.Columns },
          { name: 'InstanceNumber', value: ctNumber },
        ];
        ctMetaData.push(readCtItem);
      }
      // add only one MR information
      if (mrItem) {
        let readMrItem = [
          { name: 'Modality', value: { modalities: mrItem.modalities, seriesIUID: null } },
          { name: 'StudyDateTime', value: mrItem.StudyDate + ' ' + mrItem.StudyTime },
          { name: 'SeriesInstanceUID', value: mrItem.SeriesInstanceUID },
          { name: 'Manufacturer', value: mrItem.Manufacturer },
          { name: 'BitsAllocated', value: mrItem.BitsAllocated },
          { name: 'Columns', value: mrItem.Columns },
          { name: 'ImageOrientationPatient', value: mrItem.ImageOrientationPatient },
          { name: 'ImagePositionPatient', value: mrItem.ImagePositionPatient },
          { name: 'PatientPosition', value: mrItem.PatientPosition },
          { name: 'Rows', value: mrItem.Rows },
          { name: 'Columns', value: mrItem.Columns },
          { name: 'InstanceNumber', value: mrNumber },
        ];
        mrMetaData.push(readMrItem);
      }

      // add only one MR information
      if (ptItem) {
        let readPtItem = [
          { name: 'Modality', value: { modalities: ptItem.modalities, seriesIUID: null } },
          { name: 'StudyDateTime', value: ptItem.StudyDate + ' ' + ptItem.StudyTime },
          { name: 'SeriesInstanceUID', value: ptItem.SeriesInstanceUID },
          { name: 'Manufacturer', value: ptItem.Manufacturer },
          { name: 'Columns', value: ptItem.Columns },
          { name: 'ImageOrientationPatient', value: ptItem.ImageOrientationPatient },
          { name: 'ImagePositionPatient', value: ptItem.ImagePositionPatient },
          { name: 'PatientPosition', value: ptItem.PatientPosition },
          { name: 'Rows', value: ptItem.Rows },
          { name: 'Columns', value: ptItem.Columns },
          { name: 'InstanceNumber', value: ptNumber },
        ];
        ptMetaData.push(readPtItem);
      }
    }
    return { ctMetaData, doseMetaData, rtStructureMetaData, planMetaData, regMetaData, mrMetaData, ptMetaData };
  }

  renderStudyContent(metaData) {
    return this.props.studyMetaData ? (
      <div className="studyContentTableDiv">
        {this.renderImageMetaData(metaData.ctMetaData, 'ct')}
        {this.renderImageMetaData(metaData.mrMetaData, 'mr')}
        {this.renderImageMetaData(metaData.ptMetaData, 'pt')}
        {this.renderDoseMetaData(metaData.doseMetaData)}
        {this.renderPlanMetaData(metaData.planMetaData)}
        {this.renderRegMetaData(metaData.regMetaData)}
        {this.renderRTStructureMetaData(metaData.rtStructureMetaData)}
      </div>
    ) : (
      <LinearProgress className="linearProgress"></LinearProgress>
    );
  }

  async downloadStudyZip() {
    this.setState({ isDownloading: true });
    const openPatientName = this.props.studies?.find((study) => study.StudyInstanceUID == this.props.openedStudyContentUID)?.PatientName;
    const contourModificationHistoryDataSet = new ContourModificationHistoryDataSet();
    const iid = await contourModificationHistoryDataSet.getOrthancInstanceID(this.props.openedStudyContentUID);
    try {
      // Download the dicom file
      const link = document.createElement('a');
      link.href = this.state.href;
      link.download = '';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
      // Download the histories data
      const data = await contourModificationHistoryDataSet.getAllContourModifications(this.props.openedStudyContentUID);
      if (data) {
        const fileName = iid + '-' + openPatientName + '-modificationhistory';
        const json = JSON.stringify(data);
        const blob = new Blob([json], { type: 'application/json' });
        const href = await URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = href;
        link.download = fileName + '.json';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    } catch (exc) {
      console.log('Download study zipfile: Blob method failed with the following exception. ');
      console.log('Error message: ' + exc);
    } finally {
      contourModificationHistoryDataSet.delete();
    }
  }

  render() {
    return (
      <div>
        <Modal
          className="studyContentDialog"
          visible={this.state.visible}
          destroyOnClose={true}
          closable={false}
          onOk={() => this.handleOk()}
          footer={[
            <Button
              key="exportZip"
              style={{ marginRight: '20px' }}
              disabled={this.props.studyMetaData ? false : true}
              onClick={async () => this.downloadStudyZip()}
            >
              Download ZIP
            </Button>,
            <Button key="ok" onClick={() => this.handleOk()}>
              {' '}
              OK
            </Button>,
          ]}
        >
          <h2 className="studyContentTitle" style={{ textAlign: 'center' }}>
            Study Content
          </h2>
          {this.renderStudyContent(this.parseMetaData())}
        </Modal>
        <Modal
          className="downloadingZIPModal"
          zIndex={1000}
          style={{ width: 'fit-content' }}
          closable={false}
          destroyOnClose={true}
          setTimeout={setTimeout(() => {
            if (this._isMounted) {
              this.setState({ isDownloading: false });
            }
          }, 10000)}
          open={this.state.isDownloading}
          footer={null}
        >
          <h3 className="downloadingZIPlabel" style={{ color: 'yellow', textAlign: 'center', fontSize: '25px' }}>
            <h3>Download ZIP: You can see the progress ongoing in the browser</h3>
          </h3>
        </Modal>
        <Modal
          className="confirmDeleteSingleDicomDialog"
          closable={false}
          destroyOnClose={true}
          open={this.state.isConfirmingDeleteDicomFile}
          onCancel={() =>
            this.setState({
              isConfirmingDeleteDicomFile: false,
            })
          }
          onOk={() => {
            this.setState({ isConfirmingDeleteDicomFile: false });
            this.handleConfirmedDeleteDicomFile();
          }}
          style={{ width: 'fit-content' }}
        >
          <h3 className="confirmDeleteDicomFile" style={{ textAlign: 'center', color: 'yellow', fontSize: '25px' }}>
            Do you really want to delete the DICOM file?
          </h3>
        </Modal>
      </div>
    );
  }
}
