export async function getFileContent(currentFilePath) {
  const response = await fetch('/aimodels/file', {
    method: 'POST',
    body: JSON.stringify({ path: currentFilePath }),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  if (response.status == 200) {
    const data = await response.json();
    return data.content;
  } else {
    throw new Error(response.statusText);
  }
}

export async function saveAiModelInformation(modelInfo) {
  const response = await fetch('/aimodels/file', {
    method: 'POST',
    body: JSON.stringify({ data: modelInfo }),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  if (response.status == 200) {
    return 'Saving of model information successfully';
  } else {
    throw new Error(response.statusText);
  }
}
